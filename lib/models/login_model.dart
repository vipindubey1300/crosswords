// To parse this JSON data, do
//
//     final loginModel = loginModelFromJson(jsonString);

import 'dart:convert';

LoginModel loginModelFromJson(String str) => LoginModel.fromJson(json.decode(str));

String loginModelToJson(LoginModel data) => json.encode(data.toJson());

class LoginModel {

  bool status;
  String message;
  Result result;

  LoginModel({
    this.status,
    this.message,
    this.result
  });

  factory LoginModel.fromJson(Map<String, dynamic> json) => LoginModel(
    status: json["status"],
    message: json["message"],
    result : Result.fromJson(json['result'])
  );

  Map<String, dynamic> toJson() => {

    "status": status,
    "message": message,
    'result':result

  };
}


class Result {
  String id;
  String status;
  String email;
  String first_name;
  String mobile;
  String profile_pic;
  String jwt_token;

  Result({
    this.id,
    this.status,
    this.email,
    this.first_name,
    this.mobile,
    this.profile_pic,
    this.jwt_token,

  });

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    id: json["id"].toString(),
    status: json["status"].toString(),
    email: json["email"],
    first_name: json["first_name"],
    mobile: json["mobile"],
    profile_pic: json["profile_pic"],
    jwt_token: json["jwt_token"],

  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "status": status,
    "email": email,
    "first_name": first_name,
    "mobile": mobile,
    "profile_pic": profile_pic,
  };
}
