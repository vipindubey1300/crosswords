
import 'dart:convert';

CheckModel loginModelFromJson(String str) => CheckModel.fromJson(json.decode(str));
String loginModelToJson(CheckModel data) => json.encode(data.toJson());

class CheckModel {
  bool status;
  String message;
  CheckModel({
    this.status,
    this.message,

  });

  factory CheckModel.fromJson(Map<String, dynamic> json) => CheckModel(
      status: json["status"],
      message: json["message"],

  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message
  };
}


