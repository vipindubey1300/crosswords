
import 'dart:convert';
class NotificationResult {
  bool status;
  List<Notifications> notifications;



  NotificationResult({
    this.status,
    this.notifications,

  });

  factory NotificationResult.fromJson(Map<String, dynamic> json) => NotificationResult(
    status: json["status"],
    notifications: (json["result"] as List).map((e) => Notifications.fromJson(e)).toList(),
  );

}


class Notifications {

  String message;
  String added_date;


  Notifications({
    this.message,
    this.added_date,
  });

  factory Notifications.fromJson(Map<String, dynamic> json) => Notifications(

    message: json["message"],
    added_date: json["added_date"],
  );

}



