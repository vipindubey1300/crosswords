
import 'dart:convert';

class ResultHome {
 Result result;

  ResultHome({
    this.result,
  });

  factory ResultHome.fromJson(Map<String, dynamic> json) => ResultHome(
    result: Result.fromJson(json["result"]),
  );

}

class Result {
  bool status;
  List<Sliders> sliders;
  List<Categories> categories;


  Result({
    this.status,
    this.sliders,
    this.categories,

  });

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    status: json["status"],
    sliders: (json["sliders"] as List).map((e) => Sliders.fromJson(e)).toList(),
    categories:  (json["categories"] as List).map((e) => Categories.fromJson(e)).toList(),

  );

}
class Sliders {
  String id;
  String slider_image;
  String slider_title;
  String slider_description;
  String start_date;

  Sliders({
    this.id,
    this.slider_image,
    this.slider_title,
    this.slider_description,
    this.start_date,
  });

  factory Sliders.fromJson(Map<String, dynamic> json) => Sliders(
    id: json["id"].toString(),
    slider_image: json["slider_image"],
    slider_title: json["slider_title"],
    slider_description: json["slider_description"],
    start_date: json["start_date"],

  );

}
class Categories {
  String id;
  String name;
  String image;


  Categories({
    this.id,
    this.name,
    this.image,
  });

  factory Categories.fromJson(Map<String, dynamic> json) => Categories(
    id: json["id"].toString(),
    name: json["name"],
    image: json["image"],
  );

}
