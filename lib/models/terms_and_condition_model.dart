// To parse this JSON data, do
//
//     final loginModel = loginModelFromJson(jsonString);

import 'dart:convert';

TermsModel termsModelFromJson(String str) => TermsModel.fromJson(json.decode(str));

String termsModelToJson(TermsModel data) => json.encode(data.toJson());

class TermsModel {
  bool status;
  Result result;

  TermsModel({
    this.status,
    this.result
  });

  factory TermsModel.fromJson(Map<String, dynamic> json) => TermsModel(
      status: json["status"],
      result : Result.fromJson(json['result'])
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    'result':result
  };
}


class Result {
  String page_title;
  String page_description;

  Result({
    this.page_title,
    this.page_description,

  });

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    page_title: json["page_title"],
    page_description: json["page_description"]
  );

  Map<String, dynamic> toJson() => {
    "page_title": page_title,
    "page_description": page_description,
  };
}
