
import 'dart:convert';

class ResultPodcastDetails{
  Result result;

  ResultPodcastDetails({
    this.result,
  });

  factory ResultPodcastDetails.fromJson(Map<String, dynamic> json) => ResultPodcastDetails(
    result: Result.fromJson(json["result"]),
  );

}

class Result {
  bool status;
  List<Episodes> episodes;
  Podcast podcast;


  Result({
    this.status,
    this.episodes,
    this.podcast
  });

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    status: json["status"],
    podcast:  Podcast.fromJson(json["podcast"]),
    episodes: (json["episodes"] as List).map((e) => Episodes.fromJson(e)).toList(),


  );

}


class Episodes {
  String id;
  String episode_name;
  String episode_image;
  String episode_audio;


  Episodes({
    this.id,
    this.episode_name,
    this.episode_image,
    this.episode_audio,

  });

  factory Episodes.fromJson(Map<String, dynamic> json) => Episodes(
    id: json["id"].toString(),
    episode_name: json["episode_name"],
    episode_image: json["episode_image"],
    episode_audio: json["episode_audio"],
  );

}

class Podcast {
  String id;
  String podcast_image;
  String name;
  String description;
  String podcast_file;

  Podcast({
    this.id,
    this.podcast_image,
    this.name,
    this.description,
    this.podcast_file,
  });

  factory Podcast.fromJson(Map<String, dynamic> json) => Podcast(
    id: json["id"].toString(),
    podcast_image: json["podcast_image"],
    name: json["name"],
    description: json["description"],
    podcast_file: json["podcast_file"],

  );

}

