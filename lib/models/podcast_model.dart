
import 'dart:convert';

class ResultPodcast{
  Result result;

  ResultPodcast({
    this.result,
  });

  factory ResultPodcast.fromJson(Map<String, dynamic> json) => ResultPodcast(
    result: Result.fromJson(json["result"]),
  );

}

class Result {
  bool status;
  List<Podcasts> podcasts;

  Result({
    this.status,
    this.podcasts,

  });

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    status: json["status"],
    podcasts: (json["podcasts"] as List).map((e) => Podcasts.fromJson(e)).toList(),

  );

}


class Podcasts {
  String id;
  String podcast_image;
  String name;
  String description;
  String podcast_file;

  Podcasts({
    this.id,
    this.podcast_image,
    this.name,
    this.description,
    this.podcast_file,
  });

  factory Podcasts.fromJson(Map<String, dynamic> json) => Podcasts(
    id: json["id"].toString(),
    podcast_image: json["podcast_image"],
    name: json["name"],
    description: json["description"],
    podcast_file: json["podcast_file"],

  );

}

