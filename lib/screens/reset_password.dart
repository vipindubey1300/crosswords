import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'dart:io' show Platform;
import 'package:http/http.dart' as http;

import 'package:crosswords/helpers/common_methods.dart';
import 'package:crosswords/helpers/app_colors.dart';
import 'package:crosswords/models/check_model.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'package:crosswords/helpers/app_constants.dart';
import 'dart:convert';
import 'package:crosswords/models/login_model.dart';
import 'package:crosswords/widgets/progress_bar.dart';
import 'package:crosswords/helpers/common_methods.dart';



class ResetPassword extends StatefulWidget {
  final String email;

  ResetPassword({Key key,   @required this.email,this.title}) : super(key: key);

  final String title;
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ResetPasswordState();
  }
}

class _ResetPasswordState extends State<ResetPassword> {

  var commonMethod = new CommonMethods();


  final newPasswordController = TextEditingController();
  final confirmPasswordController = TextEditingController();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String _email,_new_password,_confirm_password;
  bool _loadingState = false;


  void _resetPassword(newpassword,confirmpassword) async {
    String url = AppConstants.BASE_URL + 'auth/reset_password';
    setState(() {_loadingState = true;});

    final response = await http.post(url, body: {'email':widget.email,'password':newpassword,'confirm_password':confirmpassword});
    setState(() {_loadingState = false;});
    try {
      var responseJson = jsonDecode(response.body);
      var res = CheckModel.fromJson(responseJson);
      print(response.body);
      if(res.status) {
        commonMethod.showMessage(res.message);
        Navigator.pushNamed(context, '/login');
      }else{
        commonMethod.showMessage(res.message,error: true);
      }

    } catch (e) {
      commonMethod.showMessage(e.toString(),error: true);
    }

  }

  void isValid(){

    _new_password = newPasswordController.text;
    _confirm_password=confirmPasswordController.text;


    if (newPasswordController.text.isEmpty){
      commonMethod.showMessage("Please Enter Password",error: true);
    }
    else if (newPasswordController.text.length<6 || newPasswordController.text.length>16 ){
      commonMethod.showMessage("Please Enter Valid Password",error: true);
    }
    else if (confirmPasswordController.text.isEmpty){
      commonMethod.showMessage("Please Enter Confirm Password",error: true);

    } else if (confirmPasswordController.text.length<6 || confirmPasswordController.text.length>16){
      commonMethod.showMessage("Please Enter Valid Confirm Password",error: true);
    }
    else if (_confirm_password !=_new_password){
      commonMethod.showMessage("Password Not Match",error: true);
    }
    else {
      _resetPassword(_new_password, _confirm_password);
    }



  }


  @override
  Widget build(BuildContext context) {
    ProgressDialog pr;
    pr = new ProgressDialog(context);
    final emailController = TextEditingController(text: widget.email);

    final emailField = Container(
        height: 50,
        width: MediaQuery.of(context).size.width/1.32,
        decoration: new BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.transparent,
        ),

        child:Padding (
            padding:const EdgeInsets.fromLTRB(10, 0,0, 0),
            child:TextFormField(
              enabled: false,

              obscureText: false,
              controller: emailController,
              keyboardType: TextInputType.emailAddress,
              style: TextStyle( fontSize: 18,color: Colors.white,),
              onSaved: (input) {
                // _email = input;
              },
              decoration:InputDecoration(
                contentPadding: EdgeInsets.all(10),
                border: InputBorder.none,
                hintStyle: TextStyle(fontSize: 18.0, color: Colors.grey,fontFamily: 'OpenSans'),
                hintText: "Email Address",

              ),//  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
            ))) ;
    final newPassword = Container(
        height: 50,
        width: MediaQuery.of(context).size.width/1.32,
        decoration: new BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.transparent,
        ),

        child:Padding (
            padding:const EdgeInsets.fromLTRB(10, 0,0, 0),
            child:TextFormField(
              obscureText: true,
              controller: newPasswordController,
              keyboardType: TextInputType.visiblePassword,
              style: TextStyle( fontSize: 20,color: Colors.white),
              onSaved: (input) {
                // _email = input;
              },
              decoration:InputDecoration(
                contentPadding: EdgeInsets.all(10),
                border: InputBorder.none,
                hintStyle: TextStyle(fontSize: 18.0, color: Colors.grey,fontFamily: 'OpenSans'),
                hintText: "New Password",

              ),//  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
            ))) ;


    final confirmPassword = Container(
        height: 50,
        width: MediaQuery.of(context).size.width/1.32,
        decoration: new BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.transparent,
        ),

        child:Padding (
            padding:const EdgeInsets.fromLTRB(10, 0,0, 0),
            child:TextFormField(
              onFieldSubmitted: (_) =>this.isValid(),
              obscureText: true,
              controller: confirmPasswordController,
              keyboardType: TextInputType.visiblePassword,
              style: TextStyle( fontSize: 20,color: Colors.white),
              onSaved: (input) {
                // _email = input;
              },
              decoration:InputDecoration(
                contentPadding: EdgeInsets.all(10),
                border: InputBorder.none,
                hintStyle: TextStyle(fontSize: 18.0, color: Colors.grey,fontFamily: 'OpenSans'),
                hintText: "Confirm Password",

              ),//  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
            ))) ;
    final loginButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(15.0),
      color: Color(0xff18ACEF),
      child: MaterialButton(
        height: 50,
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        onPressed: () {


        },
        child: Text("Submit",
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 22),
        ),
      ),
    );
    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Reset Password'),
        backgroundColor: AppColors.PRIMARY_COLOR,
      ),
      body: new Stack(
          children: <Widget>[
            new Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(image: new AssetImage("assets/images/forgotPassword.png"), fit: BoxFit.cover,),
              ),
            ),

            SingleChildScrollView(

              child: Container(
                margin: EdgeInsets.fromLTRB(0, 40, 0, 0),
                child: new Column(
                  children: <Widget>[
                    Container
                      (
                      width: MediaQuery.of(context).size.width,
                      height: 150,
                      child:Image.asset("assets/images/logoimage.png"),) ,
                    Container(
                      margin: const EdgeInsets.fromLTRB(20,0, 20, 60),
                      width: MediaQuery.of(context).size.width,
                      height:  210,
                      decoration: new BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black,
                            blurRadius: 30.0, // has the effect of softening the shadow
                            spreadRadius: 2.0, // has the effect of extending the shadow
                            offset: Offset(
                              10.0, // horizontal, move right 10
                              10.0, // vertical, move down 10
                            ),
                          ),],
                        color:AppColors.PRIMARY_COLOR,
                        borderRadius: new BorderRadius.circular(25.0),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [  //total container height is 150
                          SizedBox (height: 15.0),
                          emailField, //height is 50
                          Padding (
                            padding:const EdgeInsets.fromLTRB(10, 0,10, 0),
                            child:Divider(
                              color: Colors.white,
                              thickness: 0.5,
                              height: 2,
                            ),
                          ),
                          newPassword,
                          Padding (
                            padding:const EdgeInsets.fromLTRB(10, 0,10, 0),
                            child:Divider(
                              color: Colors.white,
                              thickness: 0.5,
                              height: 2,

                            ),
                          ),
                          confirmPassword,
                          Padding (
                            padding:const EdgeInsets.fromLTRB(10, 0,10, 0),
                            child:Divider(
                              color: Colors.white,
                              thickness: 0.5,
                              height: 2,

                            ),
                          ),
                          SizedBox (height: 15.0),
                        ],
                      ),
                    ),
                    new GestureDetector(
                      child: new Container(
                        margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        width: 180,
                        height: 60,
                        child:new Padding(
                          padding: EdgeInsets.fromLTRB(5, 5, 5,5),
                          child:Center(child: Text(
                            'Send',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 19, fontFamily: 'Rock Salt',fontWeight: FontWeight.bold,

                            ),
                          ),),
                        ) ,
                        decoration: new BoxDecoration(
                          borderRadius:BorderRadius.circular(15.0),
                          image: new DecorationImage(
                            image: new AssetImage("assets/images/buttongradient.png",), fit: BoxFit.fill,
                          ),

                        ),
                      ),
                      onTap: ()
                      {
                      this.isValid();

                      },
                    ),

                  ],
                ),
              ),
            ),

            _loadingState ?  getProgressBar() : Container()



          ]
      ),
    );
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value,style: TextStyle(color: Colors.white, fontSize: 15.0),),
      backgroundColor: Colors.red,
    ));
  }

}
class  EmptyAppBar  extends StatelessWidget implements PreferredSizeWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
  @override
  Size get preferredSize => Size(0.0,0.0);
}
