import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'dart:io' show Platform;
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:crosswords/helpers/app_constants.dart';
import 'package:crosswords/widgets/progress_bar.dart';

import 'package:crosswords/helpers/app_colors.dart';
import 'dart:convert';

import 'package:crosswords/helpers/common_methods.dart';
import 'package:crosswords/screens/login.dart';


class Wallet extends StatefulWidget {

  Wallet({Key key, this.title}) : super(key: key);

  final String title;
  @override
  _WalletState createState() => new _WalletState();
}

class _WalletState extends State<Wallet> {

  var commonMethod = new CommonMethods();


  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String coins = "0";
  bool _loadingState = false;

  void _fetchWalletDetails() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('jwt_token');
    String url = AppConstants.BASE_URL + 'dashboard/wallet?auth=' + token;
    setState(() {_loadingState = true;});
    final response = await http.get(url);
    try {
      var responseJson = jsonDecode(response.body);

      setState(() {_loadingState = false;});

      print(responseJson['result']);

      if(responseJson['status']){
        var coinsTemp = responseJson['result'];
        this.setState(() { coins = coinsTemp.toString();});
      }
    } catch (e) {
      print(e.toString());
      commonMethod.showMessage('Error in fetching puzzles.. ',error: true);
    }
  }

  @override
  void initState() {
    super.initState();
    this._fetchWalletDetails();
  }

  @override
  Widget build(BuildContext context) {


    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomPadding: false,
      appBar: GradientAppBar(
        centerTitle: true,
        title: Text('Wallet'),
        gradient: LinearGradient(
            colors: [AppColors.PRIMARY_COLOR, AppColors.PRIMARY_COLOR, Colors.black]),
      ),
      body: new Stack(
          children: <Widget>[
            new Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(image: new AssetImage("assets/images/forgotPassword.png"), fit: BoxFit.cover,),
              ),
            ),
            SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  _loadingState ? 
                      Padding(
                        
                        child: getSmallProgressBar(),
                        padding: EdgeInsets.all(20),
                      ):
                  Container(
                    margin: const EdgeInsets.only(top: 30.0,left: 10,right: 10,bottom: 10),
                    width: MediaQuery.of(context).size.width,
                    decoration: new BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black,
                          blurRadius: 30.0, // has the effect of softening the shadow
                          spreadRadius: 2.0, // has the effect of extending the shadow
                          offset: Offset(
                            10.0, // horizontal, move right 10
                            10.0, // vertical, move down 10
                          ),
                        ),],
                      color: AppColors.PRIMARY_COLOR,
                      borderRadius: new BorderRadius.circular(10.0),
                    ),
                    child:Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Expanded(
                            child: Container(
                              child:  Image.asset('assets/images/bitcoin.png',width: 40,height: 40,),
                            ),
                            flex: 2,
                          ),

                          new Expanded(
                            child: Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text('Total wallet balance',style: TextStyle(color: Colors.white,fontSize: 17,fontWeight: FontWeight.bold),),
                                      Text('COINS',style: TextStyle(color: Colors.grey),)
                                    ],
                                  ),
                                  SizedBox(height: 17,),
                                  Text('${coins} COINS',style: TextStyle(color: Colors.white,fontSize: 27,fontWeight: FontWeight.w500),),
                                  SizedBox(height: 5,),
                                 // Text("19.2 USD ",style: TextStyle(color: Colors.white,fontSize: 14),),



                                ],
                              ),
                            ),
                            flex: 9,
                          ),
                        ],
                      ),
                    )
                  ),



                  Align(
                    alignment: Alignment.centerRight,
                    child: Material(
                      color: Colors.transparent,
                      child: InkWell(
                        child: Padding(
                          padding: EdgeInsets.all(10.0),
                          child:Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Image.asset('assets/images/add-account.png',width: 17,height: 17,),
                              SizedBox(width: 5,),
                              Text('Add Account',style: TextStyle(color: Colors.white),)
                            ],
                          ),
                        ),
                        onTap: (){
                         Navigator.pushNamed(context, '/add_account');
                        },
                      ),
                    )
                  )
                ],
              ),
            ),

          ]
      ),
    );
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value,style: TextStyle(color: Colors.white, fontSize: 15.0),),
      backgroundColor: Colors.red,
    ));
  }

}
class  EmptyAppBar  extends StatelessWidget implements PreferredSizeWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
  @override
  Size get preferredSize => Size(0.0,0.0);
}