import 'dart:async';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';


import 'package:crosswords/helpers/app_colors.dart';

class Splash extends StatefulWidget {

  @override
  _SplashState createState() => new _SplashState();
}

class _SplashState extends State<Splash>{
  startTime() async {
    var _duration = new Duration(seconds: 2);
    return new Timer(_duration, navigationPage);
  }

  void navigationPage() async{


    SharedPreferences prefs = await SharedPreferences.getInstance();
    var  loggedIn = prefs.getString('user_id');

    if(loggedIn == null)
      Navigator.pushReplacementNamed(context, '/login');
    else
     // Navigator.pushReplacementNamed(context, '/home');
      Navigator.pushReplacementNamed(context, '/game');
  }

  @override
  void initState() {
    super.initState();
    startTime();

  }

  @override
  Widget build(BuildContext context) {

    return new Scaffold(
      body: new Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(image: new AssetImage("assets/images/splash-bg.png"), fit: BoxFit.cover,),
        ),
        child: Center(
          child: Image.asset("assets/images/wordSearchLogo.gif",height: 5000,width: 5000,
          ),
        ),
      )
    );
  }
}