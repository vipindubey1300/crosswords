import 'dart:math';

import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'dart:io' show Platform;
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

import 'package:crosswords/helpers/app_colors.dart';

import 'package:crosswords/helpers/common_methods.dart';
import 'package:crosswords/screens/login.dart';

import 'package:crosswords/helpers/common_methods.dart';
import 'package:crosswords/screens/reset_password.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'package:crosswords/helpers/app_constants.dart';
import 'dart:convert';
import 'package:crosswords/models/check_model.dart';
import 'package:crosswords/widgets/progress_bar.dart';
import 'package:crosswords/helpers/common_methods.dart';


class Support extends StatefulWidget {

  Support({Key key, this.title}) : super(key: key);
  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".



  final String title;
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SupportState();
  }
}

class _SupportState extends State<Support> {

  var commonMethod = new CommonMethods();

  final firstNameController = TextEditingController();
  final  lastNameController = TextEditingController();
  final emailAddressController = TextEditingController();
  final phoneNumberController = TextEditingController();
  final messageController = TextEditingController();

  bool _loadingState = false;

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String _first,_last,_message,_number,_email;

  void _support() async {
    var fname  = firstNameController.text;
    var lname = lastNameController.text;
    var email =emailAddressController.text;
    var phone  = phoneNumberController.text;
    var message = messageController.text;

    String url = AppConstants.BASE_URL + 'dashboard/support';
    setState(() {_loadingState = true;});

    final response = await http.post(url, body: {
      'first_name':fname,
      'last_name':lname,
      'email':email,
      'mobile':phone,
      'message':message,});
    setState(() {_loadingState = false;});
    try {
      var responseJson = jsonDecode(response.body);
      var res = CheckModel.fromJson(responseJson);
      print(response.body);
      if(res.status) {
        commonMethod.showMessage(res.message);
        Navigator.pushNamed(context, '/home');
      }else{
        commonMethod.showMessage(res.message,error: true);
      }

    } catch (e) {
      commonMethod.showMessage(e.toString(),error: true);
    }

  }


  void isValid(){
    var fname  = firstNameController.text;
    var lname = lastNameController.text;
    var email =emailAddressController.text;
    var phone  = phoneNumberController.text;
    var message = messageController.text;

    FocusScope.of(context).requestFocus(FocusNode());

    if (fname.isEmpty){
      commonMethod.showMessage("Please Enter First Name",error: true);
    }
    else if (lname.isEmpty){
      commonMethod.showMessage("Please Enter Last Name",error: true);

    }else if (email.isEmpty){
      commonMethod.showMessage("Enter Email");
    }
    else if(commonMethod.validateEmail (email)!="Valid")
    {
      commonMethod.showMessage("Please Enter Valid Email Id",error: true);
    }
    else if (phone.isEmpty){
      commonMethod.showMessage("Enter Mobile Number",error: true);
    }
    else if (phone.length < 10){
      commonMethod.showMessage("Please Enter Valid Mobile Number",error: true);
    }
    else if (message.isEmpty){
      commonMethod.showMessage("Please Enter Message",error: true);
    }
    else {
    _support();

    }



  }

  @override
  Widget build(BuildContext context) {

    final firstName = Container(
        height: 50,
        decoration: new BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.transparent,
        ),

        child:Padding (
            padding:const EdgeInsets.fromLTRB(17, 5,0, 0),
            child:TextFormField(
              controller:firstNameController,
              keyboardType: TextInputType.text,
              style: TextStyle( fontSize: 20,color: Colors.white),
              onSaved: (input) {
                // _email = input;
              },
              decoration:InputDecoration(
                contentPadding: EdgeInsets.fromLTRB(10,0,0,0),
                border: InputBorder.none,
                hintStyle: TextStyle(fontSize: 18.0, color: Colors.grey,fontFamily: 'OpenSans'),
                hintText: "First Name",

              ),//  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
            ))) ;
    final lastName = Container(
        height: 50,
        decoration: new BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.transparent,
        ),

        child:Padding (
            padding:const EdgeInsets.fromLTRB(17, 5,0, 0),
            child:TextFormField(

              keyboardType: TextInputType.text,
              controller:lastNameController,

              style: TextStyle( fontSize: 20,color: Colors.white),
              onSaved: (input) {
                // _email = input;
              },
              decoration:InputDecoration(
                contentPadding: EdgeInsets.fromLTRB(10, 0,0, 0),
                border: InputBorder.none,
                hintStyle: TextStyle(fontSize: 18.0, color: Colors.grey,fontFamily: 'OpenSans'),
                hintText: "Last Name",

              ),//  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
            ))) ;
    final emailAddress = Container(
        height: 50,
        decoration: new BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.transparent,
        ),

        child:Padding (
            padding:const EdgeInsets.fromLTRB(15, 0,0, 0),
            child:TextFormField(
              keyboardType: TextInputType.emailAddress,
              controller:emailAddressController,

              style: TextStyle( fontSize: 20,color: Colors.white),
              onSaved: (input) {
                // _email = input;
              },
              decoration:InputDecoration(
                contentPadding: EdgeInsets.all(10),
                border: InputBorder.none,
                hintStyle: TextStyle(fontSize: 18.0, color: Colors.grey,fontFamily: 'OpenSans'),
                hintText: "Email Address",

              ),//  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
            ))) ;
    final phoneNumber = Container(
        height: 50,
        decoration: new BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.transparent,
        ),

        child:Padding (
            padding:const EdgeInsets.fromLTRB(15, 0,0, 0),
            child:TextFormField(
              keyboardType: TextInputType.phone,
              controller:phoneNumberController,

              style: TextStyle( fontSize: 20,color: Colors.white),
              onSaved: (input) {
                // _email = input;
              },
              decoration:InputDecoration(
                contentPadding: EdgeInsets.all(10),
                border: InputBorder.none,
                hintStyle: TextStyle(fontSize: 18.0, color: Colors.grey,fontFamily: 'OpenSans'),
                hintText: "Enter Phone Number",

              ),//  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
            ))) ;
    final message = Container(
        height: 80,
        decoration: new BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.transparent,
        ),

        child:Padding (
            padding:const EdgeInsets.fromLTRB(15, 0,0, 0),
            child:TextFormField(
              controller:messageController,
              keyboardType: TextInputType.multiline,
              maxLines: null,

              style: TextStyle( fontSize: 20,color: Colors.white),
              decoration:InputDecoration(
                contentPadding: EdgeInsets.all(10),
                border: InputBorder.none,
                hintStyle: TextStyle(fontSize: 18.0, color: Colors.grey,fontFamily: 'OpenSans'),
                hintText: "Enter Message",

              ),//  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
            )
        )) ;

    final loginButon = GestureDetector(
      child: new Container(
        margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
        width: 180,
        height: 50,
        child:new Padding(
          padding: EdgeInsets.fromLTRB(5, 5, 5,5),
          child:Center(child: Text(
            'Submit',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontSize: 19, fontFamily: 'Rock Salt',fontWeight: FontWeight.bold,

            ),
          ),),
        ) ,
        decoration: new BoxDecoration(
          borderRadius:BorderRadius.circular(15.0),
          image: new DecorationImage(
            image: new AssetImage("assets/images/buttongradient.png",), fit: BoxFit.fill,
          ),

        ),
      ),
      onTap: ()
      {
        this.isValid();

      },
    );
    return Scaffold(
      backgroundColor: AppColors.PRIMARY_COLOR,
      key: _scaffoldKey,
     // resizeToAvoidBottomPadding: false,
      appBar: GradientAppBar(
        centerTitle: true,
        title: Text('Support'),
        gradient: LinearGradient(
            colors: [AppColors.PRIMARY_COLOR, AppColors.PRIMARY_COLOR, Colors.black]),
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        behavior: HitTestBehavior.translucent,
        child: new Stack(
            children: <Widget>[
              new Container(
                decoration: new BoxDecoration(
                  image: new DecorationImage(image: new AssetImage("assets/images/forgotPassword.png"), fit: BoxFit.cover,),
                ),
              ),

              SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Container(
                        margin: const EdgeInsets.only(top: 40.0,left: 10,right: 10,bottom: 10),
                        width: MediaQuery.of(context).size.width,
                        decoration: new BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black,
                              blurRadius: 30.0, // has the effect of softening the shadow
                              spreadRadius: 2.0, // has the effect of extending the shadow
                              offset: Offset(
                                10.0, // horizontal, move right 10
                                10.0, // vertical, move down 10
                              ),
                            ),],
                          color: AppColors.PRIMARY_COLOR,
                          borderRadius: new BorderRadius.circular(10.0),
                        ),
                        child:Padding(
                            padding: EdgeInsets.all(10.0),
                            child: Column(
                              children: <Widget>[
                                Text('Support ',style: TextStyle(color: Colors.blue,fontSize: 17,fontWeight: FontWeight.bold),),

                                SizedBox (height: 8.0),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 1,
                                      child:Column(
                                        children: <Widget>[
                                          firstName,
                                          Padding (
                                            padding:const EdgeInsets.fromLTRB(15, 0,15, 0),
                                            child:Divider(
                                              color: Colors.grey,
                                              thickness: 0.6,
                                              height: 1,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Expanded(
                                      flex: 1,
                                      child: Column(
                                        children: <Widget>[
                                          lastName,
                                          Padding (
                                            padding:const EdgeInsets.fromLTRB(15, 0,15, 0),
                                            child:Divider(
                                              color: Colors.grey,
                                              thickness: 0.6,
                                              height: 1,
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ), //height is 50



                                SizedBox (height: 8.0),
                                emailAddress, //height is 50
                                Padding (
                                  padding:const EdgeInsets.fromLTRB(15, 0,15, 0),
                                  child:Divider(
                                    color: Colors.grey,
                                    thickness: 0.6,
                                    height: 1,
                                  ),
                                ),

                                SizedBox (height: 8.0),
                                phoneNumber, //height is 50
                                Padding (
                                  padding:const EdgeInsets.fromLTRB(15, 0,15, 0),
                                  child:Divider(
                                    color: Colors.grey,
                                    thickness: 0.6,
                                    height: 1,
                                  ),
                                ),

                                SizedBox (height: 8.0),
                                message, //height is 50
                                Padding (
                                  padding:const EdgeInsets.fromLTRB(15, 0,15, 0),
                                  child:Divider(
                                    color: Colors.grey,
                                    thickness: 0.6,
                                    height: 1,
                                  ),
                                ),





                              ],
                            )
                        )
                    ),
                    SizedBox(height: 30.0,),
                    Center(
                        child: loginButon
                    )


                  ],
                ),
              ),
              _loadingState ?  getProgressBar() : Container()





            ]
        ),
      ),
    );
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value,style: TextStyle(color: Colors.white, fontSize: 15.0),),
      backgroundColor: Colors.red,
    ));
  }

}