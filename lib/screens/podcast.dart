import 'package:crosswords/screens/podcast_details.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'dart:io' show Platform;
import 'package:http/http.dart' as http;
import 'package:crosswords/helpers/app_colors.dart';

import 'package:crosswords/helpers/common_methods.dart';
import 'package:crosswords/screens/reset_password.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'package:crosswords/helpers/app_constants.dart';
import 'dart:convert';
import 'package:crosswords/models/podcast_model.dart';
import 'package:crosswords/models/check_model.dart';

import 'package:crosswords/widgets/progress_bar.dart';
import 'package:crosswords/helpers/common_methods.dart';


class Podcast extends StatefulWidget {

  Podcast({Key key, this.title,this.id}) : super(key: key);
  final String title;
  final String id;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _PodcastState();
  }
}

class _PodcastState extends State<Podcast> {

  var commonMethod = new CommonMethods();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  bool _loadingState= false;

  Future<ResultPodcast> getDetail() async{
    setState(() {
      _loadingState = true;
    });

    String url = AppConstants.BASE_URL + 'dashboard/podcasts?category_id='+ widget.id;
    print(url);
    final response = await http.get(url);
    print(response.body);
    return ResultPodcast.fromJson(jsonDecode(response.body));
  }





  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomPadding: false,
      appBar: GradientAppBar(
        centerTitle: true,
        title: Text(widget.title),
        gradient: LinearGradient(
            colors: [AppColors.PRIMARY_COLOR, AppColors.PRIMARY_COLOR, Colors.black]),
      ),
      body: new Stack(
          children: <Widget>[
            new Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(image: new AssetImage("assets/images/forgotPassword.png"), fit: BoxFit.cover,),
              ),
            ),

            new  FutureBuilder<ResultPodcast>(
              future:getDetail(),
              builder: (context, snapshot) {

                if (snapshot.hasData) {
                  return this.makeView(snapshot.data.result);
                } else if (snapshot.hasError) {
                  return Text("${snapshot.error}");
                }
                // By default, show a loading spinner.
                return Container(
                  height: MediaQuery.of(context).size.height,
                  child: Center(
                      child: getSmallProgressBar()
                  ),

                );
              },
            )




          ]
      ),
    );
  }

  Widget makeView(data){

    var homeView =   new GridView.builder(
      itemCount: data.podcasts.length,
      shrinkWrap: true,
      gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2,
        childAspectRatio: MediaQuery.of(context).size.width /
            (MediaQuery.of(context).size.height/1.9 ),),
      physics: NeverScrollableScrollPhysics() ,
      itemBuilder: (BuildContext context, int index) {
        return Container(
            margin: EdgeInsets.all(4),
            decoration: new BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.transparent,
            ),
            child:new GestureDetector(
              child: Stack(
                children: <Widget>[
                  Positioned(
                    top: 0,bottom: 0,right: 0,left: 0,
                    child: Image.network (
                      AppConstants.BASE_URL_IMAGE + data.podcasts[index].podcast_image,
                      fit: BoxFit.fill,
                    ),
                  ),
                  Positioned(
                    bottom: 10,left: 10,
                    child:Container(
                      height: 30,width: 30,child:  Image.asset('assets/images/play.png',),
                    ),
                  )
                ],
              ),

              onTap: () {
              // commonMethod.showMessage('Coming soon' );
               Navigator.push(context, new MaterialPageRoute(
                 builder: (BuildContext context) => new PodcastDetails(id: data.podcasts[index].id),
               ));
              },

            ));
      },
    );
    return homeView;
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value,style: TextStyle(color: Colors.white, fontSize: 15.0),),
      backgroundColor: Colors.red,
    ));
  }

}