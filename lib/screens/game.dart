import 'package:date_format/date_format.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'dart:io' show Platform;
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:crosswords/helpers/app_constants.dart';
import 'package:crosswords/helpers/app_colors.dart';
import 'package:crosswords/helpers/common_methods.dart';
import 'package:crosswords/screens/crosswords.dart';
import 'package:crosswords/widgets/word_marker.dart';
import 'package:crosswords/screens/navigation_drawer.dart';
import 'package:crosswords/widgets/countdown_timer.dart';
import 'package:flutter/services.dart';
import 'dart:async';
import 'dart:convert';
import 'package:crosswords/helpers/crossword_controller.dart';
import 'package:crosswords/widgets/progress_bar.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:video_player/video_player.dart';
import 'package:chewie/chewie.dart';


class Game extends StatefulWidget {

  Game({Key key, this.title}) : super(key: key);

  final String title;
  @override
  _GameState createState() => new _GameState();
}

class _GameState extends State<Game>  with SingleTickerProviderStateMixin {

  final CrosswordController myController = CrosswordController();

  var commonMethod = new CommonMethods();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final GlobalKey<_GameState> _myWidgetState = GlobalKey<_GameState>();

  int wordsPerLine = 11;


  List<String> alphabet = [];
  List<String> words = [];

  String gameImage = " ";

  final markers = <WordMarker>[];
  int correctAnswers = 0;
  var uniqueLetters;

  bool isTimeCompleted = false;
  bool _loadingState = false;

  var gameTime = 3;

  Timer _timer;
  int _start = 3;
  bool _gameLoading = false;

  void startCountdownTimer() {
    const oneSec = const Duration(milliseconds: 1);
    _timer = new Timer.periodic(
      oneSec,
          (Timer timer) => setState(
            () {
          if (_start < 1) {
            timer.cancel();

            //start stopwatch
            MyStopWatch.start();
            startTimer();

            this.setState(() => _gameLoading = false);
          } else {
            _start = _start - 1;
          }
        },
      ),
    );
  }


  AnimationController _controller;

  String stopTimeToDisplay = "00:00:00";
  Stopwatch MyStopWatch = Stopwatch();
  final dur = const Duration(milliseconds: 1);

  void startTimer(){
    Timer(dur, keepRunning);
  }
  transformMilliSeconds(int milliseconds) {
    //Thanks to Andrew
    int hundreds = (milliseconds / 10).truncate();
    int seconds = (hundreds / 100).truncate();
    int minutes = (seconds / 60).truncate();

    String minutesStr = (minutes % 60).toString().padLeft(2, '0');
    String secondsStr = (seconds % 60).toString().padLeft(2, '0');
    String miliSecondsStr = (hundreds % 100).toString().padLeft(2, '0');

    return "$minutesStr:$secondsStr:$miliSecondsStr";
  }

  void keepRunning(){
    if(MyStopWatch.isRunning){
      startTimer();
    }
    setState(() {
//      stopTimeToDisplay = MyStopWatch.elapsed.inHours.toString().padLeft(2,"0") + ":"
//          +  (MyStopWatch.elapsed.inMinutes % 60).toString().padLeft(2,"0") + ":"
//          +  (MyStopWatch.elapsed.inSeconds % 60).toString().padLeft(2,"0");

      stopTimeToDisplay = transformMilliSeconds(MyStopWatch.elapsedMilliseconds);
    });
  }

  Future getPuzzles() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('jwt_token');
    String url = AppConstants.BASE_URL + 'dashboard/puzzle?auth=' + token;
    setState(() {_loadingState = true;});
    print(url);
    final response = await http.get(url);
    setState(() {_loadingState = false;});
    try {
      var responseJson = jsonDecode(response.body);

     print(responseJson);

      if(responseJson['status']){

       startCountdownTimer();
       this.setState(() => _gameLoading = true);


       // _controller.forward();
          setState(() {
              gameImage = responseJson['image'];
              alphabet = responseJson['result']['chars'].cast<String>();
              words = responseJson['result']['words'].cast<String>(); //.cast<String>(); to avoid List<dynamic>' is not a subtype of type 'List<String>
          });

      }
      else{
        setState(() {_loadingState = false;});

        commonMethod.showMessage(responseJson['message']);
      }
    } catch (e) {
      print(e.toString());
      commonMethod.showMessage('Error in fetching puzzles.. ',error: true);
    }
  }

  void _gameEnd(String score) async {

    setState(() {_loadingState = true;});

    String url = AppConstants.BASE_URL + 'dashboard/gameend';
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('jwt_token');
    print(score);
    var body =  {
      'game_score':score,
      'auth':token,
    };


    final response = await http.post(url,body:body);
    print(response.body);
    setState(() {_loadingState = false;});

    try {
      var responseJson = jsonDecode(response.body);
      print(response.body);

      // print(response.body);

      if(responseJson['status']) {
        MyStopWatch.stop();
        commonMethod.showMessage('Submitted Score Successfully..',error: false);

      }else{
         commonMethod.showMessage('Score Submission Failed',error: true);
      }

    } catch (e) {
      commonMethod.showMessage(e.toString(),error: true);
    }
  }

  var chewieController = null;
  var videoPlayerController = null;


  Widget makeVideoStreaming(){
    videoPlayerController = VideoPlayerController.network("https://www.webmobril.org/dev/crossword/uploads/video.mp4");
    chewieController = ChewieController(
      videoPlayerController: videoPlayerController,
      aspectRatio: 2/ 2,
      autoPlay: true,
      looping: true,
      //fullScreenByDefault: true,
      allowFullScreen: true,
      showControls: false,
      allowMuting: true,
    );

    chewieController.setVolume(0.0);


  }




  @override
  void initState() {
    super.initState();

    this.getPuzzles();
    this.makeVideoStreaming();

    _controller =  AnimationController(vsync: this, duration: Duration(seconds: gameTime));
    _controller.addStatusListener(((status) {
      if (status == AnimationStatus.completed) {

        var score = myController.getScore().toString();
       // print("completed----------------"+ score);
       // this._gameEnd('$score');
        setState(() {
          isTimeCompleted = true;
        });


      } else if (status == AnimationStatus.dismissed) {
        //controller.forward();
      }
    }));

  }



  @override
  void dispose(){

    print('DISPOSE CALLED- GAME---');

    _timer.cancel();
    MyStopWatch.stop();

    videoPlayerController.dispose();
    chewieController.dispose();
    _controller.dispose();


    super.dispose();

  }

  some(){
    print("_______");
  }


  showTime(){
   List time= stopTimeToDisplay.split(':');
    return (
    Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[Text(time[0] + ":" ,style: TextStyle(color: Colors.blue,fontSize: 21,fontWeight: FontWeight.bold),),
        Text(time[1]  + ":" ,style: TextStyle(color: Colors.blue,fontSize: 21,fontWeight: FontWeight.bold),),
        Text(time[2],style: TextStyle(color: Colors.lightBlue,fontSize: 18,fontWeight: FontWeight.w500),)],
    )
    );
  }



  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child:  Scaffold(
        key: _scaffoldKey,
        drawer: NavigationDrawer(),
        resizeToAvoidBottomPadding: false,

        appBar: GradientAppBar(
          centerTitle: true,
          iconTheme: new IconThemeData(color: Colors.black),//to change drawer icon color
          title:  showTime(),
          gradient: LinearGradient(
              colors: [Colors.white, Colors.white, Colors.white,]
          ),
          actions: <Widget>[
            new SizedBox(
              child: IconButton(
                padding: new EdgeInsets.all(8.0),
                icon: Icon(Icons.account_balance_wallet, size: 20,color: Colors.black,),
                onPressed: () {
                },
              ),
            ),
          ],
        ),
        body:   SingleChildScrollView(
          child: Stack(
            children: <Widget>[
              Container(
                color: Colors.white,
                child: Column(
                  children: <Widget>[
                    /**
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        SizedBox(height: 25,),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[

                            IconButton(
                              icon: Icon(Icons.access_time, size: _gameLoading ? 15 : 22 ,color: Colors.black,),
                              onPressed: () {
                                // Navigator.pushNamed(context, '/game');
                              },
                            ),

                            new Container(
                              padding: EdgeInsets.all(10),
                              color: Colors.black,
                              child: Text(stopTimeToDisplay,style: TextStyle(color: Colors.red,fontSize: 21,fontWeight: FontWeight.bold),),
//                            child: isTimeCompleted ? Text("Time Out",style: TextStyle(color: Colors.redAccent,fontSize: 21,fontWeight: FontWeight.bold),) :Countdown(
//                              animation: StepTween(
//                                begin: gameTime, // convert to seconds here
//                                end: 0,
//                              ).animate(_controller),
//                            )
                            ),
                          ],
                        ),
                        _gameLoading ?  ClipRRect(
                          borderRadius: BorderRadius.circular(8.0),
                          child: Image.network(
                            AppConstants.BASE_URL_IMAGE + gameImage,
                            width: 60.0,
                            height: 60.0,
                            fit: BoxFit.fill,
                          ),
                        ) : Container(),
                        _gameLoading ?  Container(
                          margin: EdgeInsets.fromLTRB(0, 10, 10, 0),
                          height: 80,
                          width: 80,
                          color: Colors.white,
//                          child: ClipRRect(
//                            borderRadius: BorderRadius.circular(8.0),
//                            child: Image.network(
//                              AppConstants.BASE_URL_IMAGE + gameImage,
//                              width: 95.0,
//                              height: 95.0,
//                              fit: BoxFit.fill,
//                            ),
//                          ),
                          child: Chewie(
                            controller: chewieController,
                          ),
                        ): Container()

                      ],
                    ),

                        **/
                    SizedBox(height: 20,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Text('Word Match',style: TextStyle(color: Colors.black,fontSize: 22,fontWeight: FontWeight.bold),),
                        RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                              side: BorderSide(color: Colors.blue)),
                          onPressed: () {
                            var score = myController.getScore().toString();
                           print("completed----------------"+ score);
                           this._gameEnd('$score');
                          },
                          color: Colors.blue,
                          textColor: Colors.white,
                          child: Text("Submit game".toUpperCase(),
                              style: TextStyle(fontSize: 14,color: Colors.white)),
                        ),
                      ],
                    ),



                    (alphabet.length == 0 || words.length == 0 )
                        ? Padding(
                      padding: EdgeInsets.all(30),
                      child: getSmallProgressBar(),
                    ):
                    Crosswords(alphabet: alphabet,words: words,controller: myController,),
                    SizedBox(height: 20,),




                  ],
                ),
              ),
               _gameLoading ? Positioned(
                  top: 0,left: 0,right: 0,bottom: 0,
                  child: Container(
                      color: Colors.black12,
                      child:Center(
                        child: Text("Game Starting in $_start seconds",style: TextStyle(color: Colors.blue,fontSize: 50,),textAlign: TextAlign.center,)
                      )
                  )
              ) : Container()
            ],
          ),
        ),
      ),
    );
  }




  Future<bool> _onBackPressed() {

    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text("Confirm Exit"),
        content: Text("Are you sure you want to exit?"),
        actions: <Widget>[
          FlatButton(
            child: Text("YES"),
            onPressed: () {
              SystemNavigator.pop();
            },
          ),
          FlatButton(
            child: Text("NO"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          )
        ],
      ),
    ) ??
        false;
  }

}