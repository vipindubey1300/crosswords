import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'dart:io' show Platform;
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:crosswords/helpers/app_constants.dart';
import 'package:crosswords/helpers/app_colors.dart';
import 'package:crosswords/helpers/common_methods.dart';
import 'package:crosswords/screens/login.dart';
import 'package:crosswords/widgets/word_marker.dart';
import 'package:crosswords/helpers/crossword_controller.dart';
import 'dart:math';



class Crosswords extends StatefulWidget {
  const Crosswords({Key key, this.alphabet, this.words,this.controller}) : super(key: key);

  final List<String> alphabet;
  final List<String> words;
  final CrosswordController controller;


  @override
  _CrosswordsState createState() => _CrosswordsState(controller);
}

class _CrosswordsState extends State<Crosswords> {
  _CrosswordsState(CrosswordController _controller) {
    _controller.getScore = getScore;
  }

  int getScore() {
    return selectedLetters.length;
  }

  List<bool> isSelected = [];
  List<TextDecoration> decor = [TextDecoration.lineThrough,TextDecoration.lineThrough,TextDecoration.lineThrough,TextDecoration.lineThrough];
  List<String> selectedLetters = [];
  //final markers = <WordMarker>[];
  Map<GlobalKey, String> lettersMap;

  List<Map<String, dynamic>> markers =[];

  List<Offset> selectedWordPosition=[];



  final crossWord = <Positioned>[];



  Offset initialTappedPosition = Offset(0, 0);
  Offset initialPosition = Offset(0, 0);


//  Offset initialPositionSelectedWord = Offset(0, 0);
//  Offset finalPositionSelectedWord = Offset(0, 0);




  Offset finalPosition;

  int initialSquare;
  int index = -1;
  int crossAxisCount = 11; //whether you use GridView or not still need to provide this
  bool isTapped = false;

  String selectedWord = '';

  double width = 20;
  double height = 20;
  Size size;



  @override
  void dispose(){
    print('DISPOSE CROSSWRDSSSSSSSSSSSS----');
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    lettersMap = Map.fromIterable(widget.alphabet, key: (i) => GlobalKey(), value: (i) => i[0]);
    isSelected = List.generate(widget.alphabet.length, (e) => false);
  }

  void addMarker(Rect rect, int startIndex) {
//    markers.add(WordMarker(
//      rect: rect,
//      startIndex: startIndex,
//    ));
  }

  WordMarker adjustedMarker(WordMarker originalMarker, Rect endRect) {
    return originalMarker.copyWith(rect: originalMarker.rect.expandToInclude(endRect));
  }

  Color getColor(){
    Random rnd;
    int min = 1;
    int max = 4;
    rnd = new Random();
    var r = min + rnd.nextInt(max - min);
    print("$r is in the range of $min and $max");

    if(r == 1){
      return  Color.fromRGBO(0,0,255,0.4);
    }
    else if(r == 2){
      return  Color.fromRGBO(0,255,0,0.4);
    }
    else if(r == 3){
      return Color.fromRGBO(255,0,0,0.4);
    }

     return Color.fromRGBO(0,0,255,0.4);
  }
  void addCross(var top, var left ,var height ,var width,bool isHorizontal, bool isVertical) {
    print('selected weord postions  ${selectedWordPosition}');
//    print('isHorizontal ${isHorizontal}');
//    print('isVertical ${isVertical}');
    print('top ${top}');
    print('left ${left}');
//    print('initial position X ${initialPosition.dx}');
//    print('initial position Y ${initialPosition.dy}');
//
//
//    print('final position X ${finalPosition.dx}');
//    print('final position Y ${finalPosition.dy}');

    var temp ={
      'top':top,
      'left':left,
      'height':height,
      'width':width
    };

    var tempHeight =height  + (isVertical ? 10 : 0);
    var tempWidth = width  + (isHorizontal ? 13 : 0);


    print('tempHeight ```````````````${tempHeight}');
    print('tempWidth`````````````` ${tempWidth}');

    print('top  ------>>>>>   ${ top + (isHorizontal ? 40 : 50)}');
    print('left ----- >>>>    ${left + (isVertical ? 40 : 50)}');


    var t_height = (selectedWordPosition[selectedWordPosition.length-1].dy.abs() - selectedWordPosition[0].dy.abs()) +20;
    print('t_height *************${tempHeight}');


    var widget =   Positioned(
//      top: top + (isHorizontal ? 40 : 40),
//      left:left + (isVertical ? 40 : 37),


//
//      top:  (isHorizontal ? (selectedWordPosition[0].dy.abs()+17) :top + 40),
//      left: (isVertical ? (selectedWordPosition[0].dx.abs()-5) :left + 37 ),


      top:  (isHorizontal ? (selectedWordPosition[0].dy.abs()+17) :(selectedWordPosition[0].dy.abs()+17)),
      left: (isVertical ? (selectedWordPosition[0].dx.abs()+25) : (selectedWordPosition[0].dx.abs()+20) ),


      child: Container(

//        height:height  + (isVertical ? 10 : 0) ,
//        width: width  + (isHorizontal ? 13 : 0) ,

          height: t_height,
           width: width  + (isHorizontal ? 13 : 0) ,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
           color:getColor()
//            boxShadow: [
//              BoxShadow(color: Colors.green, spreadRadius: 3),
//            ],
          ),

      ),
    );
    crossWord.add(widget);
  }


  _determineWord(BuildContext context) {
    double difference;
    int numberOfSquares;

    var isHorizontal = false;
    var isVertical = false;

    //first reset all values
    setState(() {
      selectedWord = '';
      isSelected = List.generate(widget.alphabet.length, (e) => false);
      selectedWordPosition=[];
    });

   // print('inital square...... ${initialSquare}');


    if ((finalPosition.dx - initialPosition.dx) > 20) {
      print('right');

      //moved right
      difference = finalPosition.dx - initialPosition.dx;
      numberOfSquares = (difference / size.width).ceil();

      for (int i = initialSquare ; i < (initialSquare + numberOfSquares); i++) {
       // print('right index---- ${i}');
         isSelected[i] = true;
      }
      for (int i = 0; i < isSelected.length; i++) {
        if (isSelected[i]) {
             selectedWord += widget.alphabet[i];
               var key = lettersMap.keys.toList()[i];
               RenderBox box = key.currentContext.findRenderObject();
               Offset position = box.globalToLocal(Offset.zero,ancestor: context.findRenderObject());
               //print('Final word start poistion -------- ${position}');
               selectedWordPosition.add(position);

        }
      }

      isHorizontal = true;
     // print('selecetd word dragged from right ${selectedWord}');
    } else if ((initialPosition.dx - finalPosition.dx) > 20) {
      print('left');

      // moved left
      difference =   initialPosition.dx  - finalPosition.dx;
      numberOfSquares = (difference / size.width).ceil();
//
//      print('left difference---- ${difference}');
//      print('left size.width---- ${size.width}');
//      print('left numberOfSquares---- ${numberOfSquares}');
//      print('left initialSquare---- ${initialSquare}');
//      print('left final count---- ${(initialSquare - numberOfSquares + 1)}');

      for (int i = initialSquare ; i >= (initialSquare - numberOfSquares + 1); i--) {
        i > 0 ? (isSelected[i] = true) : null;
      }
      for (int i = 0; i < isSelected.length; i++) {
        if (isSelected[i]) {
          selectedWord += widget.alphabet[i];

          var key = lettersMap.keys.toList()[i];
          RenderBox box = key.currentContext.findRenderObject();
          Offset position = box.globalToLocal(Offset.zero,ancestor: context.findRenderObject());

          selectedWordPosition.add(position);
        }
      }
      isHorizontal = true;
      print('selecetd word dragged from left ${selectedWord}');
    } else if ((finalPosition.dy - initialPosition.dy) > 20) {
      //moved up when moving up/down number of squares numberOfSquares is also number of rows

      difference = finalPosition.dy - initialPosition.dy;
      numberOfSquares = (difference / size.height).ceil();

      for (int i = initialSquare  ; i < (initialSquare + (numberOfSquares * crossAxisCount)); i += crossAxisCount) {

       // print('move down index---- ${i}');
        isSelected[i] = true;
      }
      for (int i = 0; i < isSelected.length; i++) {
        if (isSelected[i]) {
          selectedWord += widget.alphabet[i];

          var key = lettersMap.keys.toList()[i];
          RenderBox box = key.currentContext.findRenderObject();
          Offset position = box.globalToLocal(Offset.zero,ancestor: context.findRenderObject());
          selectedWordPosition.add(position);
        }
      }
      isVertical = true;
      print('selecetd word dragged from move up ${selectedWord}');
    }
    else if ((initialPosition.dy - finalPosition.dy) > 20) {
      //moved down
      difference = initialPosition.dy - finalPosition.dy;
      numberOfSquares = (difference / size.height).ceil();
      for (int i = initialSquare ; i > (initialSquare - (numberOfSquares * crossAxisCount)); i -= crossAxisCount) {
       // print('move down index---- ${i}');
        isSelected[i] = true;
      }
      for (int i = isSelected.length - 1; i >= 0; i--) {
        if (isSelected[i]) {
          selectedWord += widget.alphabet[i];

          var key = lettersMap.keys.toList()[i];
          RenderBox box = key.currentContext.findRenderObject();
          Offset position = box.globalToLocal(Offset.zero,ancestor: context.findRenderObject());
          selectedWordPosition.add(position);

        }
      }
      isVertical = true;
      print('selecetd word dragged from move down ${selectedWord}');
    }
      setState(() {
        selectedWord = selectedWord;
      });

//    RenderBox box = key.currentContext.findRenderObject();
//    Offset position = box.localToGlobal(Offset.zero); //this is global position
//    double y = position.dy;

    //this is y - I think it's what you want
    print('Final word -- ${selectedWord}');

   //this is global position


   // print('Final word Postion -- ${position}');


    if(widget.words.contains(selectedWord)){

      if(!selectedLetters.contains(selectedWord)){
        this.setState(() {selectedLetters.add(selectedWord);});
        //make markers
        var top =  initialTappedPosition.dy;
        var left =  initialTappedPosition.dx;
        var markerHeight = height;
        var markerWidht = width;


        addCross(top, left, markerHeight, markerWidht,isHorizontal,isVertical);

        print('makrer top--- ${top}');
        print('makrer left--- ${left}');
        print('makrer heifht--- ${markerHeight}');
        print('makrer markerWidhtmarkerWidht--- ${markerWidht}');
      }
      


    }

    //reset height width
    setState(() {
      height = 20;
      width = 20;
    });

  }

  List <Widget> makeMarkers()
  {
    final tempMarker = <Positioned>[];
   // tempMarker.add(value)
    var item = markers;

   var arr = item.map((e) {
      // get index
      var index = item.indexOf(e);
         tempMarker.add(
             Positioned(
              top: item[index]['top'],
              left: item[index]['left'],
              child: Container(
              height: item[index]['height'],
              width: item[index]['width'],
              decoration: ShapeDecoration(
              shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30),
              side: BorderSide(
                color: isTapped ? Colors.yellow : Colors.transparent,
                width: 1.0,
              ),
            ),
          ),
        ),
      ));

       }).toList();
   // return new Row(children: strings.map((item) => new Text(item)).toList());
    return tempMarker;
  }

  final GlobalKey _globalKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
//    print('initialTappedPosition DX  -- ${initialPosition.dx}');
//    print('initialTappedPosition DY  -- ${initialPosition.dy}');
//    print('HEIGHT  -- ${height}');
//    print('WIDHT  -- ${width}');
    return Column(
      children: <Widget>[
        Container(
            margin: const EdgeInsets.only(top: 10.0,left: 10,right: 10,bottom: 0),
            width: MediaQuery.of(context).size.width,
            decoration: new BoxDecoration(
//              color: AppColors.PRIMARY_COLOR_TRANSPARENT,
              borderRadius: new BorderRadius.circular(10.0),
            ),
            child:Container(
            decoration: new BoxDecoration(
            //   color: AppColors.PRIMARY_COLOR_TRANSPARENT,
            borderRadius: new BorderRadius.circular(10.0),
            border: Border.all(
            color: Colors.black, // <--- border color
            width: 0.6,
            ),
         ),
                padding: EdgeInsets.all(0.0),
                child: Stack(
                  children: <Widget>[

                    Center(
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: GestureDetector(
                          child: GridView(
                            key: _globalKey,
                            physics: NeverScrollableScrollPhysics(),
                            //Very Important if
                            // you don't have this line you will have conflicting touch inputs and with
                            // gridview being the child will win
                            shrinkWrap: true,
                            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: crossAxisCount,
                              childAspectRatio: 1,
                            ),
                            children: <Widget>[
                              for (int i = 0; i != lettersMap.length; ++i)

                                  Container(
                                    key: lettersMap.keys.toList()[i],
                                    child: Listener(
                                      child: Text(
                                        lettersMap.values.toList()[i],
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 15,
                                        ),
                                      ),
                                        onPointerDown: (PointerDownEvent event) {
//                                    print(lettersMap.keys.toList()[i]);
//                                    print(i);
                                          final RenderBox renderBox = lettersMap.keys
                                              .toList()[i]
                                              .currentContext
                                              .findRenderObject();
                                          size = renderBox.size;
                                          print('Size width --- ${size.width}');
                                          print('Size height --- ${size.height}');
                                          setState(() {
                                            isSelected[i] = true;
                                            initialSquare = i;
                                          });
                                        }
                                    )
                                  ),


                            ],
                          ),
                          onTapDown: (TapDownDetails details) {
                            //User Taps Screen
                            //  print('Global Position: ${details.globalPosition}');
                            setState(() {
                              initialPosition = Offset(
                                details.localPosition.dx - 25,
                                details.localPosition.dy - 25,
                              );
                              initialTappedPosition = Offset(
                                details.localPosition.dx - 25,
                                details.localPosition.dy - 25,
                              );
                              isTapped = true;
                            });
                            // print(initialPosition);
                          },
                          //  DELTA The amount the pointer has moved in the coordinate space of the event receiver since the previous update
                          onVerticalDragUpdate: (DragUpdateDetails details) {
                            // print('${details.delta.dy}');
                            setState(() {
                              if (details.delta.dy < 0) {
                                initialTappedPosition = Offset(initialTappedPosition.dx, initialTappedPosition.dy + details.delta.dy);
                                height -= details.delta.dy;
                              } else {
                                height += details.delta.dy;
                              }
                              finalPosition = Offset(
                                details.localPosition.dx - 25,
                                details.localPosition.dy - 25,
                              );
                            });
                          },
                          onHorizontalDragUpdate: (DragUpdateDetails details) {
                          // print('****************** details delte dx${details.delta.dx}');
                           //print('****************** initialTappedPosition dx ${initialTappedPosition.dx}');
                            setState(() {
                              if (details.delta.dx < 0) {
                                initialTappedPosition = Offset(
                                  initialTappedPosition.dx + details.delta.dx,
                                  initialTappedPosition.dy,
                                );
                                width -= details.delta.dx;
                              } else {
                                width += details.delta.dx;
                              }

                              finalPosition = Offset(
                                details.localPosition.dx - 25,
                                details.localPosition.dy - 25,
                              );
                            });
                          },
                          onHorizontalDragEnd: (DragEndDetails details) {
                            // print('details onHorizontalDragEnd ${details} ');
                            _determineWord(_globalKey.currentContext);
                          },
                          onVerticalDragEnd: (DragEndDetails details) {
                            // print('details onVerticalDragEnd ${details} ');
                            _determineWord(_globalKey.currentContext);
                          },
                        ),
                      ),
                    ),
                    ...crossWord
                    //  markers.length > 0 ? makeMarkers().toList() : Spacer()
                  ],
                )
            )
        ),
        Container(
          margin: const EdgeInsets.only(top: 0.0,left: 10,right: 10,bottom: 10),
          width: MediaQuery.of(context).size.width,
          decoration: new BoxDecoration(
         //   color: AppColors.PRIMARY_COLOR_TRANSPARENT,
            borderRadius: new BorderRadius.circular(10.0),
            border: Border.all(
              color: Colors.black, // <--- border color
              width: 2.0,
            ),
          ),
          child: GridView(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount:5,
                childAspectRatio: 3
            ),
            children: <Widget>[
              ...widget.words.map((letter) =>
                  GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      child: Padding(
                        padding: EdgeInsets.only(top:5),
                        child: Text(
                          letter.toUpperCase(),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize:12,
                              fontWeight: FontWeight.w300,
                              decorationThickness: 2.85, //strike through line thickness
                              decoration:selectedLetters.contains(letter.toUpperCase()) ? TextDecoration.lineThrough : TextDecoration.none,


                          ),
                        ),
                      )

                  )
              ).toList(),
            ],
          ),
        ),

//        Align(
//          alignment: Alignment.centerLeft,
//          child: Container(
//              margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
//              height: 30,
//              width: 80,
//              decoration: ShapeDecoration(
//                  shape: RoundedRectangleBorder(
//                    borderRadius: BorderRadius.circular(30),
//                    side: BorderSide(
//                        color: Colors.blue , width: 3.0),
//                  ),
//                  color: Colors.blue
//              ),
//              child:Center(
//                child: Text(' ${selectedLetters.length} / ${widget.words.length}',style: TextStyle(color: Colors.white),),
//              )
//          ),
//        ),

      ],
    );
  }


}

