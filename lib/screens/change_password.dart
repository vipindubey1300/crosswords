import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'dart:io' show Platform;
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

import 'package:crosswords/helpers/app_colors.dart';

import 'package:crosswords/helpers/common_methods.dart';
import 'package:crosswords/screens/login.dart';
import 'package:crosswords/helpers/app_constants.dart';
import 'dart:convert';
import 'package:crosswords/models/login_model.dart';
import 'package:crosswords/widgets/progress_bar.dart';
import 'package:crosswords/helpers/common_methods.dart';
import 'package:crosswords/models/check_model.dart';


class ChangePassword extends StatefulWidget {

  ChangePassword({Key key, this.title}) : super(key: key);
  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".



  final String title;
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ChangePasswordState();
  }
}

class _ChangePasswordState extends State<ChangePassword> {

  var commonMethod = new CommonMethods();

  final oldPasswordController = TextEditingController();
  final newPasswordController = TextEditingController();
  final confirmPasswordController = TextEditingController();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String _old_password,_new_password,_confirm_password;
  bool _loadingState = false;


  removeValues() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove("user_id");
    prefs.remove('name');
    prefs.remove('email');
    prefs.remove('mobile');
    prefs.remove('jwt_token');


    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => Login()),
          (Route<dynamic> route) => false,
    );

  }

  @override
  void dispose(){

    print('DISPOSE CALLED----');
    debugPrint('didPopRoute...');

    super.dispose();

  }

  void _changePassword(oldpassword,newpassword,confirmpassword) async {
    String url = AppConstants.BASE_URL + 'dashboard/changepassword';
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('jwt_token');
    print(token);
    setState(() {_loadingState = true;});

    final response = await http.post(url, body: {'auth':token,'old_password':oldpassword,'password':newpassword,'confirm_password':confirmpassword});
    setState(() {_loadingState = false;});
    try {
      var responseJson = jsonDecode(response.body);
      var res = CheckModel.fromJson(responseJson);
      print(response.body);
      if(res.status) {
        commonMethod.showMessage('Password changed successfully');
       removeValues();
      }else{
        commonMethod.showMessage(res.message,error: true);
      }

    } catch (e) {
      commonMethod.showMessage(e.toString(),error: true);
    }

  }

  void isValid(){

    _old_password = oldPasswordController.text;
    _new_password = newPasswordController.text;
    _confirm_password=confirmPasswordController.text;
    //dismiss keyboard
    FocusScope.of(context).requestFocus(FocusNode());
    if (oldPasswordController.text.isEmpty){
      commonMethod.showMessage("Please Enter Old Password",error: true);
    }
    else if (oldPasswordController.text.length<6 || oldPasswordController.text.length>16 ){
      commonMethod.showMessage("Please Enter Valid Old Password",error: true);
    }
    else if (newPasswordController.text.isEmpty){
      commonMethod.showMessage("Please Enter New Password",error: true);
    }
    else if (newPasswordController.text.length<6 || newPasswordController.text.length>16 ){
      commonMethod.showMessage("Please Enter Valid  New Password",error: true);
    }
    else if (confirmPasswordController.text.isEmpty){
      commonMethod.showMessage("Please Enter Confirm Password",error: true);
    }
    else if (confirmPasswordController.text.length<6 || confirmPasswordController.text.length>16 ){
      commonMethod.showMessage("Please Enter Valid Confirm Password",error: true);
    }
    else if (_confirm_password !=_new_password){
      commonMethod.showMessage("Confirm password doesn't match with new password",error: true);
    }
    else {
      _changePassword(_old_password,_new_password, _confirm_password);
    }



  }


  @override
  Widget build(BuildContext context) {
    ProgressDialog pr;
    pr = new ProgressDialog(context);


    final oldPassword = Container(
        height: 50,
        width: MediaQuery.of(context).size.width/1.32,
        decoration: new BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.transparent,
        ),

        child:Padding (
            padding:const EdgeInsets.fromLTRB(10, 0,0, 0),
            child:TextFormField(
              obscureText: true,
              controller: oldPasswordController,

              style: TextStyle( fontSize: 20,color: Colors.white),
              onSaved: (input) {
                // _email = input;
              },
              decoration:InputDecoration(
                contentPadding: EdgeInsets.all(10),
                border: InputBorder.none,
                hintStyle: TextStyle(fontSize: 16.0, color: Colors.grey,fontFamily: 'OpenSans'),
                hintText: "Old Password",

              ),//  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
            ))) ;
    final newPassword = Container(
        height: 50,
        width: MediaQuery.of(context).size.width/1.32,
        decoration: new BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.transparent,
        ),

        child:Padding (
            padding:const EdgeInsets.fromLTRB(10, 0,0, 0),
            child:TextFormField(
              obscureText: true,
              controller: newPasswordController,
              keyboardType: TextInputType.visiblePassword,
              style: TextStyle( fontSize: 20,color: Colors.white),
              onSaved: (input) {
                // _email = input;
              },
              decoration:InputDecoration(
                contentPadding: EdgeInsets.all(10),
                border: InputBorder.none,
                hintStyle: TextStyle(fontSize: 16.0, color: Colors.grey,fontFamily: 'OpenSans'),
                hintText: "New Password",

              ),//  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
            ))) ;
    final confirmPassword = Container(
        height: 50,
        width: MediaQuery.of(context).size.width/1.32,
        decoration: new BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.transparent,
        ),

        child:Padding (
            padding:const EdgeInsets.fromLTRB(10, 0,0, 0),
            child:TextFormField(
              onFieldSubmitted: (_) =>this.isValid(),
              obscureText:true,
              controller: confirmPasswordController,
              keyboardType: TextInputType.visiblePassword,
              style: TextStyle( fontSize: 20,color: Colors.white),
              onSaved: (input) {
                // _email = input;
              },
              decoration:InputDecoration(
                contentPadding: EdgeInsets.all(10),
                border: InputBorder.none,
                hintStyle: TextStyle(fontSize: 16.0, color: Colors.grey,fontFamily: 'OpenSans'),
                hintText: "Confirm Password",

              ),//  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
            ))) ;

    return Scaffold(
      backgroundColor: AppColors.PRIMARY_COLOR,
      key: _scaffoldKey,
      //resizeToAvoidBottomInset: false,
      appBar: GradientAppBar(
        centerTitle: true,
        title: Text('Change Password'),
        gradient: LinearGradient(
            colors: [AppColors.PRIMARY_COLOR, AppColors.PRIMARY_COLOR, Colors.black]),
      ),
      body: new Stack(
          children: <Widget>[
            new Container(
              
              decoration: new BoxDecoration(
             
                image: new DecorationImage(image: new AssetImage("assets/images/forgotPassword.png"), fit: BoxFit.cover,),
              ),
            ),
            SingleChildScrollView(
             // reverse: true,
              child: Column(
                children: <Widget>[
                  Container(
                   
                    margin: const EdgeInsets.only(top: 120.0,left: 20,right: 20,bottom: 20),
                    width: MediaQuery.of(context).size.width,
                    height: 220,
                    decoration: new BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black,
                          blurRadius: 30.0, // has the effect of softening the shadow
                          spreadRadius: 2.0, // has the effect of extending the shadow
                          offset: Offset(
                            10.0, // horizontal, move right 10
                            10.0, // vertical, move down 10
                          ),
                        ),],
                      color: AppColors.PRIMARY_COLOR,
                      borderRadius: new BorderRadius.circular(25.0),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox (height: 10.0),
                        oldPassword, //height is 50
                        Padding (
                          padding:const EdgeInsets.fromLTRB(15, 0,15, 0),
                          child:Divider(
                            color: Colors.grey,
                            thickness: 0.6,
                            height: 2,
                          ),
                        ),
                        SizedBox (height: 10.0),
                        newPassword,
                        Padding (
                          padding:const EdgeInsets.fromLTRB(15, 0,15, 0),
                          child:Divider(
                            color: Colors.grey,
                            thickness: 0.6,
                            height: 2,
                          ),
                        ),
                        SizedBox (height: 10.0),
                        confirmPassword,
                        Padding (
                          padding:const EdgeInsets.fromLTRB(15, 0,15, 0),
                          child:Divider(
                            color: Colors.grey,
                            thickness: 0.6,
                            height: 2,
                          ),
                        ),
                      ],
                    ),
                  ),

                  Center(
                    child: new GestureDetector(
                      child: new Container(
                        margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        width: 180,
                        height: 60,
                        child:new Padding(
                          padding: EdgeInsets.fromLTRB(5, 5, 5,5),
                          child:Center(child: Text(
                            'Change',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 19, fontFamily: 'Rock Salt',fontWeight: FontWeight.bold,

                            ),
                          ),),
                        ) ,
                        decoration: new BoxDecoration(
                          borderRadius:BorderRadius.circular(15.0),
                          image: new DecorationImage(
                            image: new AssetImage("assets/images/buttongradient.png",), fit: BoxFit.fill,
                          ),

                        ),
                      ),
                      onTap: ()
                      {
                        this.isValid();

                      },
                    ),
                  )
                ],
              ),
            ),

            _loadingState ?  getProgressBar() : Container()


          ]
      ),
    );
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value,style: TextStyle(color: Colors.white, fontSize: 15.0),),
      backgroundColor: Colors.red,
    ));
  }

}
class  EmptyAppBar  extends StatelessWidget implements PreferredSizeWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
  @override
  Size get preferredSize => Size(0.0,0.0);
}