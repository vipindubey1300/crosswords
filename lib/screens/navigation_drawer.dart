import 'package:crosswords/helpers/app_constants.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'dart:io' show Platform;
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:crosswords/helpers/app_colors.dart';
import 'package:crosswords/helpers/common_methods.dart';

import 'package:flutter/services.dart';

import 'package:crosswords/helpers/common_methods.dart';
import 'package:crosswords/screens/game.dart';
import 'package:crosswords/screens/home.dart';
import 'package:crosswords/screens/login.dart';


var log="Log Out";
var name="";
var email="";
var image = '';


// this method is temporary will improve it by using shared prefrences only vbut with Future builder
//wHETHER TO CHHOOSE db sqlite it is your choice
Future<String> getUserInfo() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  name = prefs.getString('name');
  email = prefs.getString('email');
  image = prefs.getString('image');

}


class NavigationDrawer extends StatefulWidget {
  //const NavigationDrawer({}) : super(key: key);


  @override
  State<StatefulWidget> createState() => DrawerNav();

}

class DrawerNav extends State<NavigationDrawer> {
  var commonMethod = new CommonMethods();
  bool loggedin=false;

  removeValues() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Remove String
    prefs.remove("user_id");
    prefs.remove('name');
    prefs.remove('email');
    prefs.remove('mobile');
    prefs.remove('jwt_token');
    prefs.remove('image');
  }

  @mustCallSuper
  @override
  void initState() {
    getUserInfo();
  }



  @override
  Widget build (BuildContext context) {

    return SafeArea(
      bottom:false,
        child:  new Drawer(
          elevation: 3,
          child: new ListView(
            children: <Widget>[
              Container(
                  height:100,
                  color: AppColors.PRIMARY_COLOR,

                  child:Padding(
                      padding: EdgeInsets.all(10),
                      child:  new FutureBuilder<String>(
                        future:getUserInfo(), // async work
                        builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
                          switch (snapshot.connectionState) {
                            case ConnectionState.waiting: return new Text('Loading....');
                            default:
                              if (snapshot.hasError)
                                return new Text('Error: ${snapshot.error}');
                              else
                                return new Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                   GestureDetector(
                                     onTap: () {
                                       //  commonMethod.showMessage('Coming Soon');
                                       Navigator.pop(context); Navigator.pushNamed(context, '/edit_profile');
                                     },
                                     child:  CircleAvatar(
                                       backgroundColor:Colors.grey,radius: 30,
                                       backgroundImage: NetworkImage(AppConstants.BASE_URL_IMAGE + image,),
                                       //  backgroundImage: FadeInImage.assetNetwork(placeholder: "assets/images/avtar.png", image: nuAppConstants.BASE_URL_IMAGE + imagell),
                                     ),
                                   ),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children:  <Widget>[
                                        new Padding(
                                          padding: EdgeInsets.fromLTRB(20.0, 22.0, 0.0, 0.0),
                                          child:   InkWell(
                                              onTap: () {
                                                Navigator.pop(context); Navigator.pushNamed(context, '/edit_profile');
                                              },
                                            child:  Text(name
                                              ,textAlign: TextAlign.left,overflow: TextOverflow.ellipsis,
                                              style: TextStyle(fontSize: 20, fontWeight: FontWeight.normal, color: Colors.white,fontFamily: 'OpenSans'))
                                          ),

                                        ),
                                        SizedBox(height: 5,),
                                        // Text(token_security,textAlign: TextAlign.left, style: TextStyle(fontSize: 15, fontWeight: FontWeight.normal, color: Colors.white,fontFamily: 'OpenSans')),
                                        new Padding(
                                          padding: EdgeInsets.fromLTRB(20.0, 0.0, 0.0, 0.0),
                                          child: Text(email,textAlign: TextAlign.left,overflow: TextOverflow.ellipsis,style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal, color: Colors.grey,fontFamily: 'OpenSans')),
                                        ),


                                      ],
                                    ),
                                  ],
                                );
                          }
                        },
                      )
                  )
              ),



              Column(

                mainAxisSize: MainAxisSize.min,
                children:  <Widget>[

                  new Material(
                    child: InkWell(
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(builder: (context) =>Game()),
                              (Route<dynamic> route) => false,
                        );
                      },
                      child:Padding(
                        padding: EdgeInsets.all(10.0),
                        child:  Row(
                          children: <Widget>[
                            Icon(Icons.av_timer,color: Colors.black,size: 20,),
                            SizedBox(width: 10,),
                            new Text("Game", style: TextStyle(fontSize: 16,fontWeight: FontWeight.normal, color: Colors.black,fontFamily: 'OpenSans')),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding (
                    padding:const EdgeInsets.fromLTRB(10, 0,10, 0),
                    child:Divider(
                      height: 2,
                      color: Colors.grey,
                      thickness: 0.5,
                    ),
                  ),



                  new Material(
                    child: InkWell(
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => Home()));

                     //   Navigator.pop(context);  Navigator.pushNamed(context, '/home');
//                        Navigator.pop(context);
//                        Navigator.pushAndRemoveUntil(
//                          context,
//                          MaterialPageRoute(builder: (context) =>Home()),
//                              (Route<dynamic> route) => false,
//                        );
                      },
                      child:Padding(
                        padding: EdgeInsets.all(10.0),
                        child:  Row(
                          children: <Widget>[
                            Icon(Icons.audiotrack,color: Colors.black,size: 20,),
                            SizedBox(width: 10,),
                            new Text("Podcasts", style: TextStyle(fontSize: 16,fontWeight: FontWeight.normal, color: Colors.black,fontFamily: 'OpenSans')),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding (
                    padding:const EdgeInsets.fromLTRB(10, 0,10, 0),
                    child:Divider(
                      height: 2,
                      color: Colors.grey,
                      thickness: 0.5,
                    ),
                  ),

                  new Material(
                    child: InkWell(
                      child:Padding(
                        padding: EdgeInsets.all(10.0),
                        child:  Row(
                          children: <Widget>[
                            Icon(Icons.lock,color: Colors.black,size: 20,),
                            SizedBox(width: 10,),
                            new Text("Change Password", style: TextStyle(fontSize: 16,fontWeight: FontWeight.normal, color: Colors.black,fontFamily: 'OpenSans')),
                          ],
                        ),
                      ),
                      onTap: () {  Navigator.pop(context);   Navigator.pushNamed(context, '/change_password');},
                    ),
                  ),
                  Padding (
                    padding:const EdgeInsets.fromLTRB(10, 0,10, 0),
                    child:Divider(
                      height: 2,
                      color: Colors.grey,
                      thickness: 0.5,
                    ),
                  ),


                  new Material(
                    child: InkWell(
                      child:Padding(
                        padding: EdgeInsets.all(10.0),
                        child:  Row(
                          children: <Widget>[
                            Icon(Icons.edit,color: Colors.black,size: 20,),
                            SizedBox(width: 10,),
                            new Text("Edit Profile", style: TextStyle(fontSize: 16,fontWeight: FontWeight.normal, color: Colors.black,fontFamily: 'OpenSans')),
                          ],
                        ),
                      ),
                      onTap: () {
                    //  commonMethod.showMessage('Coming Soon');
                        Navigator.pop(context); Navigator.pushNamed(context, '/edit_profile');
                      },
                    ),
                  ),
                  Padding (
                    padding:const EdgeInsets.fromLTRB(10, 0,10, 0),
                    child:Divider(
                      height: 2,
                      color: Colors.grey,
                      thickness: 0.5,
                    ),
                  ),

                  new Material(
                    child: InkWell(
                      child:Padding(
                        padding: EdgeInsets.all(10.0),
                        child:  Row(
                          children: <Widget>[
                            Icon(Icons.account_balance_wallet,color: Colors.black,size: 20,),
                            SizedBox(width: 10,),
                            new Text("Wallet", style: TextStyle(fontSize: 16,fontWeight: FontWeight.normal, color: Colors.black,fontFamily: 'OpenSans')),
                          ],
                        ),
                      ),
                      onTap: () {
                        //commonMethod.showMessage('Coming Soon');
                       Navigator.pop(context);  Navigator.pushNamed(context, '/wallet');
                      },
                    ),
                  ),
                  Padding (
                    padding:const EdgeInsets.fromLTRB(10, 0,10, 0),
                    child:Divider(
                      height: 2,
                      color: Colors.grey,
                      thickness: 0.5,
                    ),
                  ),


                  new Material(
                    child: InkWell(
                      child:Padding(
                        padding: EdgeInsets.all(10.0),
                        child:  Row(
                          children: <Widget>[
                            Icon(Icons.notifications,color: Colors.black,size: 20,),
                            SizedBox(width: 10,),
                            new Text("Notifications", style: TextStyle(fontSize: 16,fontWeight: FontWeight.normal, color: Colors.black,fontFamily: 'OpenSans')),
                          ],
                        ),
                      ),
                      onTap: () {
                       // commonMethod.showMessage('Coming Soon');
                        Navigator.pop(context);  Navigator.pushNamed(context, '/notifications');
                      },
                    ),
                  ),
                  Padding (
                    padding:const EdgeInsets.fromLTRB(10, 0,10, 0),
                    child:Divider(
                      height: 2,
                      color: Colors.grey,
                      thickness: 0.5,
                    ),
                  ),


                  new Material(
                    child: InkWell(
                      child:Padding(
                        padding: EdgeInsets.all(10.0),
                        child:  Row(
                          children: <Widget>[
                            Icon(Icons.build,color: Colors.black,size: 20,),
                            SizedBox(width: 10,),
                            new Text("Terms And Conditions", style: TextStyle(fontSize: 16,fontWeight: FontWeight.normal, color: Colors.black,fontFamily: 'OpenSans')),
                          ],
                        ),
                      ),
                      onTap: () {   Navigator.pop(context); Navigator.pushNamed(context, '/terms_and_conditions');},
                    ),
                  ),
                  Padding (
                    padding:const EdgeInsets.fromLTRB(10, 0,10, 0),
                    child:Divider(
                      height: 2,
                      color: Colors.grey,
                      thickness: 0.5,
                    ),
                  ),

                  new Material(
                    child: InkWell(
                      child:Padding(
                        padding: EdgeInsets.all(10.0),
                        child:  Row(
                          children: <Widget>[
                            Icon(Icons.call,color: Colors.black,size: 20,),
                            SizedBox(width: 10,),
                            new Text("Support", style: TextStyle(fontSize: 16,fontWeight: FontWeight.normal, color: Colors.black,fontFamily: 'OpenSans')),
                          ],
                        ),
                      ),
                      onTap: () {
                        //commonMethod.showMessage('Coming Soon');
                        Navigator.pop(context); Navigator.pushNamed(context, '/support');
                      },
                    ),
                  ),
                  Padding (
                    padding:const EdgeInsets.fromLTRB(10, 0,10, 0),
                    child:Divider(
                      height: 2,
                      color: Colors.grey,
                      thickness: 0.5,
                    ),
                  ),

                  new Material(
                    child: InkWell(
                      child:Padding(
                        padding: EdgeInsets.all(10.0),
                        child:  Row(
                          children: <Widget>[
                            Icon(Icons.power_settings_new,color: Colors.black,size: 20,),
                            SizedBox(width: 10,),
                            new Text("Logout", style: TextStyle(fontSize: 16,fontWeight: FontWeight.normal, color: Colors.black,fontFamily: 'OpenSans')),
                          ],
                        ),
                      ),
                      onTap: () { _onBackPressed();},
                    ),
                  ),
                  Padding (
                    padding:const EdgeInsets.fromLTRB(10, 0,10, 0),
                    child:Divider(
                      height: 2,
                      color: Colors.grey,
                      thickness: 0.5,
                    ),
                  ),







                ],),

            ],
          ),

        ),

    );


  }
  Future<bool> _onBackPressed() {
    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Logout',style: TextStyle(fontWeight: FontWeight.w900),),
        content: new Text('Are you sure you want to Log Out?'),
        actions: <Widget>[
          new GestureDetector(
            onTap: () => Navigator.of(context).pop(false),
            child:    new Container(
                padding: new EdgeInsets.fromLTRB(0, 16, 20, 10),
                child: Text("NO",style: TextStyle(color: Colors.blue)),
            ),
          ),
          SizedBox(height: 16),
          new GestureDetector(
            onTap: () {
              removeValues();
              Navigator.pop(context);
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => Login()),
                    (Route<dynamic> route) => false,
              );
            },
            child:   new Container(
                padding: new EdgeInsets.fromLTRB(0, 16, 20, 10),
                 child: Text("YES",style: TextStyle(color: Colors.blue),),
            ),
          ),
        ],
      ),
    ) ??
        false;
  }
}


