import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'dart:io' show Platform;
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:crosswords/helpers/app_colors.dart';
import 'package:crosswords/helpers/common_methods.dart';
import 'package:crosswords/screens/login.dart';
import 'package:video_player/video_player.dart';
import 'package:chewie/chewie.dart';


class VideoStreaming extends StatefulWidget {

  VideoStreaming({Key key, this.videoUrl}) : super(key: key);

  final String videoUrl;
  @override
  _VideoStreamingState createState() => new _VideoStreamingState();
}

class _VideoStreamingState extends State<VideoStreaming> {

  var commonMethod = new CommonMethods();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final commentController = TextEditingController();
  ScrollController _scrollController = new ScrollController();


  // backing data
  List comments =[
    {
      'comment':'Wow ! this is a cool video'
    },
    {
      'comment':'Batmannnnnn'
    },
    {
      'comment':'The dark knight Rises....  :-)'
    },
    {
      'comment':'Here we go again'
    },
    {
      'comment':'I think batman is greater then spiderman',
    },

  ];



  var chewieController = null;
  var videoPlayerController = null;

  var videoSource = "https://www.youtube.com/watch?v=o9PY6NsB3_E";

  Widget makeVideo(){
    videoPlayerController = VideoPlayerController.network(widget.videoUrl);
    chewieController = ChewieController(
      videoPlayerController: videoPlayerController,
      aspectRatio: 1.2/ 2,
      autoPlay: true,
      looping: true,
      //fullScreenByDefault: true,
      allowFullScreen: true,
      showControls: false,
      allowMuting: true,
    );


  }

  @override                                                           // NEW
  void initState() {                                                  // NEW
    super.initState();
    this.makeVideo();// NEW
//    _scrollController = new ScrollController(                         // NEW
//      initialScrollOffset: 0.0,                                       // NEW
//      keepScrollOffset: true,                                         // NEW
//    );
  }

  void _toEnd() {                                                     // NEW
    _scrollController.animateTo(_scrollController.position.maxScrollExtent, duration: const Duration(milliseconds: 500), curve: Curves.easeOut);
    // NEW
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    //this is called when data changes .. so setstate like adding data to comment can re render it
  }



  @override
  void dispose() {

    print("-------------BEforee disponse----------------------------");

    videoPlayerController.dispose();
    chewieController.dispose();
    super.dispose();
    print("--------------After disponse-------------------");
  }



  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: _scaffoldKey,
      body: new Stack(
          children: <Widget>[

            Container(
              child: Chewie(
                controller: chewieController,
              ),
              color: Colors.black,
            ),
            Positioned(
              top: 80,left:0,right: 0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[

                  Text('   John Doe',style: TextStyle(color: Colors.white,fontWeight: FontWeight.w300,fontSize: 19),),
                  Row(
                    children: <Widget>[
                      Text("Live",style: TextStyle(color: Colors.red,fontWeight: FontWeight.bold,fontSize: 18),),
                      SizedBox(width: 10,),
                      Image.asset("assets/images/eye.png",width: 20,height: 20,),
                      SizedBox(width: 4,),
                      Text('10K   ',style: TextStyle(color: Colors.white,fontWeight: FontWeight.w300,fontSize: 16),),
                    ],
                  )
                ],
              ),
            ),


            Positioned(
              bottom: 20,left:0,right: 0,
              child: Container(
                height: MediaQuery.of(context).size.height * 0.4,
                color: Colors.transparent,
                child:Column(
                  children: <Widget>[
                    Expanded(
                      flex: 3,
                      child: Container(
                        color: Colors.transparent,
                        child: ListView.builder(
                          //reverse: true,
                          controller: _scrollController,
                          //shrinkWrap: true,
                          itemCount: comments.length,
                          itemBuilder: (BuildContext context, int index) {
                            return  Padding(
                              padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16),
                              child: Row(
                                children: <Widget>[
                                  CircleAvatar(backgroundColor: Colors.white,radius: 10,
                                    backgroundImage: AssetImage("assets/images/avtar.png"),),
                                  SizedBox(width: 10,),
                                  new Text(comments[index]['comment'],
                                    overflow: TextOverflow.ellipsis,
                                    softWrap: false,
                                    maxLines: 1,
                                    style: TextStyle(
                                        color: Colors.grey[100],
                                        fontWeight: FontWeight.w100,
                                        fontStyle: FontStyle.normal,
                                        fontSize: 15),
                                  ),
                                ],
                              ),
                            );
                          },),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Container(
                        margin: const EdgeInsets.all(15.0),
                        padding: const EdgeInsets.all(3.0),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey),
                             borderRadius: BorderRadius.all(
                                  Radius.circular(20.0) //                 <--- border radius here
                              ),
                        ),
                       child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,

                            children: <Widget>[
                              Expanded(
                                flex: 8,
                                child:
                                   TextFormField(
                                     controller: commentController,
                                      obscureText: false,
                                      keyboardType: TextInputType.text,
                                      style: TextStyle( fontSize: 20,color: Colors.white),
                                      onSaved: (input) {
                                        // _email = input;
                                      },
                                      decoration:InputDecoration(
                                        contentPadding: EdgeInsets.only(bottom: 17),
                                        border: InputBorder.none,
                                        hintStyle: TextStyle(fontSize: 15.0, color: Colors.grey,fontFamily: 'OpenSans'),
                                        hintText: "Write comment here",



                                  )
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: GestureDetector(
                                  onTap: (){
                                    //append comment to list view
                                    var comment = {'comment':commentController.text};
                                    comments.add(comment);
                                    commentController.clear();
                                    setState(() {
                                      comments = comments;
                                    });
                                    this._toEnd();
                                  },
                                  child:  Image.asset("assets/images/send.png",width: 20,height: 20,),
                                )
                              ),
                              Expanded(
                                flex: 1,
                                child:GestureDetector(
                                  child: Image.asset("assets/images/smiley4.png",width: 20,height: 20,fit: BoxFit.fitHeight,),
                                ),
                              )
                            ],
                       ),
                      ),
                    )
                  ],
                )
              )
            ),

          ]
      ),
    );
  }


}
