import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'dart:io' show Platform;
import 'package:http/http.dart' as http;
import 'dart:convert';


import 'package:crosswords/helpers/app_constants.dart';
import 'package:crosswords/helpers/common_methods.dart';
import 'package:crosswords/screens/login.dart';
import 'package:crosswords/helpers/app_colors.dart';
import 'package:crosswords/models/login_model.dart';
import 'package:crosswords/widgets/progress_bar.dart';
import 'package:crosswords/screens/verify_otp.dart';
import 'package:keyboard_avoider/keyboard_avoider.dart';


class Register extends StatefulWidget {
  Register({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _RegisterPageState createState() => new _RegisterPageState();
}

class _RegisterPageState extends State<Register> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  var _formKey = GlobalKey<FormState>();
  Widget emailField,passwordField,confirmpasswordField,mobileField,nameField,checkbox;
  var commonMethod = new CommonMethods();
  final emailController = TextEditingController();
  final nameController = TextEditingController();
  final mobileController = TextEditingController();
  final passwordController = TextEditingController();
  final confirmpasswordController = TextEditingController();
  bool _saving = false;
  String deviceName,deviceVersion,identifier;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String _name,_email,_mobileNumber,_password,_confirmpassword;
  bool checkboxValue=false;
  bool _loadingState = false;


  bool isLoggedIn = false;

  void _register(name,email,password,mobile) async {



    String url = AppConstants.BASE_URL + 'auth/register';
    setState(() {
      _loadingState = true;
    });


    final response = await http.post(
        url,
        body: {
          'name':name,
          'email':email,
          'mobile':mobile,
          'password': password,
          'device_type':Platform.isAndroid ? '1' : '2',
          'device_token':await commonMethod.getDeviceToken()
        }
    );



    setState(() {
      _loadingState = false;
    });





    if (response.statusCode == 200) {

      emailController.clear();
      passwordController.clear();
      confirmpasswordController.clear();
      nameController.clear();
      mobileController.clear();



      var responseJson = jsonDecode(response.body);
      var registerResponse = LoginModel.fromJson(responseJson);
      if(registerResponse.status) {
        commonMethod.showMessage(registerResponse.message);
      }else{
        commonMethod.showMessage(registerResponse.message,error: true);
      }


      var verifyStatus = registerResponse.result.status;
      if(verifyStatus == '0'){
        //not verified

        emailController.clear();
        passwordController.clear();
        nameController.clear();
        passwordController.clear();
        confirmpasswordController.clear();
        mobileController.clear();
        Navigator.push(context, new MaterialPageRoute(
          builder: (BuildContext context) => new VerifyOTP(email:registerResponse.result.email
              ,route:2),
        ));
      }
      else{

      }
      print('registerResponse----: $responseJson');
    } else {
      print('Request failed with status: ${response.body}.');
      var responseJson = jsonDecode(response.body);
      commonMethod.showMessage(responseJson['message']);
    }

  }

  void isValid(){

    _email = emailController.text;
    _password = passwordController.text;
    _confirmpassword=confirmpasswordController.text;
    _name = nameController.text;
    _mobileNumber = mobileController.text;


    //dismiss keyboard
    FocusScope.of(context).requestFocus(FocusNode());

    if (nameController.text.isEmpty){
      commonMethod.showMessage("Please Enter Name",error: true);
    }
    else if (!commonMethod.validateUsername(_name)){
      commonMethod.showMessage("Please Enter Valid Name",error: true);

    }else if (emailController.text.isEmpty){
      commonMethod.showMessage("Enter Email",error: true);
    }

    else if(commonMethod.validateEmail (_email)!="Valid")
    {
      commonMethod.showMessage("Please Enter Valid Email Id",error: true);
    }
    else if (mobileController.text.isEmpty){
      commonMethod.showMessage("Enter Mobile Number",error: true);
    }
    else if (_mobileNumber.length < 10){
      commonMethod.showMessage("Please Enter Valid Mobile Number",error: true);
    }
    else if (passwordController.text.isEmpty){
      commonMethod.showMessage("Please Enter Password",error: true);
    }
    else if (passwordController.text.length < 6 || passwordController.text.length>16  ) {
      commonMethod.showMessage("Please Enter Valid Password",error: true);
    }
    else if (confirmpasswordController.text.isEmpty){
      commonMethod.showMessage("Please Enter Confirm Password",error: true);
    }
    else if (confirmpasswordController.text.length<6 || confirmpasswordController.text.length>16  ) {
      commonMethod.showMessage("Please Enter Valid Confirm Password",error: true);
    }
    else if(_confirmpassword !=_password ){
      commonMethod.showMessage("Password Not Match",error: true);
    }
    else if(!checkboxValue ){
      commonMethod.showMessage("Please Accept Terms And Conditions",error: true);
    }
    else {
      _register(_name,_email, _password,_mobileNumber);

    }



  }

  @override
  Widget build(BuildContext context) {

    emailField = Container(

        height: 50,
        width: MediaQuery.of(context).size.width/1.5,
        decoration: new BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.transparent,
        ),

        child:Padding (
            padding:const EdgeInsets.fromLTRB(23, 0,0, 0),
            child:TextFormField(

              obscureText: false,
              controller: emailController,
              keyboardType: TextInputType.emailAddress,
              style: TextStyle( fontSize: 20),
              onSaved: (input) {
                // _email = input;
              },
              decoration:InputDecoration(
                contentPadding: EdgeInsets.only(top: 5.0),
                border: InputBorder.none,
                hintStyle: TextStyle(fontSize: 18.0, color: Colors.grey,fontFamily: 'OpenSans',),
                icon: new Padding(
                    padding: EdgeInsets.fromLTRB(3, 10, 0,0),
                    child:Icon(
                      Icons.email,size: 17,
                      color: Colors.grey,
                    )),
                hintText: "Email Address",

              ),//  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
            ))) ;


    passwordField = Container(
        height: 50,

        width: MediaQuery.of(context).size.width/1.5,
        decoration: new BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.white,
        ),
        child: Padding (
            padding:const EdgeInsets.fromLTRB(23, 0,0, 0),
            child: TextFormField(

              obscureText: true,
              controller: passwordController,
              style: style,
              validator: (String value){
                if (value.isEmpty){
                  return "Please enter the password";
                }
              },
              onSaved: (input) {
                //  _password = input;
              },
              decoration: InputDecoration(
                contentPadding: EdgeInsets.only(top: 5.0),
                border: InputBorder.none,
                hintStyle: TextStyle(fontSize: 18.0, color: Colors.grey,fontFamily: 'OpenSans'),
                icon: new Padding(
                    padding: EdgeInsets.fromLTRB(3, 10, 0,0),
                    child:Icon(
                      Icons.lock,size: 18,
                      color: Colors.grey,
                    )),
                hintText: "Password",
              ),

            )));

    confirmpasswordField = Container(
        height: 50,

        width: MediaQuery.of(context).size.width/1.5,
        decoration: new BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.white,
        ),
        child: Padding (

            padding:const EdgeInsets.fromLTRB(23, 0,0, 0),
            child: TextFormField(
              onFieldSubmitted: (_) =>this.isValid(),
              obscureText: true,
              controller: confirmpasswordController,
              style: style,
              validator: (String value){
                if (value.isEmpty){
                  return "Please enter the confirm  password";
                }
              },
              onSaved: (input) {
                //  _password = input;
              },
              decoration: InputDecoration(
                contentPadding: EdgeInsets.only(top: 5.0),
                border: InputBorder.none,
                hintStyle: TextStyle(fontSize: 18.0, color: Colors.grey,fontFamily: 'OpenSans'),
                icon: new Padding(
                    padding: EdgeInsets.fromLTRB(3, 10, 0,0),
                    child:Icon(
                      Icons.lock,size: 18,
                      color: Colors.grey,
                    )),
                hintText: "Confirm Password",
              ),

            )));

    mobileField = Container(
        height: 50,
        width: MediaQuery.of(context).size.width/1.5,
        decoration: new BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.white,
        ),

        child:Padding (
            padding:const EdgeInsets.fromLTRB(23,0,0, 0),
            child:TextFormField(
              maxLength: 11,
              obscureText: false,
              controller: mobileController,
              keyboardType: TextInputType.number,
              style: TextStyle(fontSize: 20),
              onSaved: (input) {
                // _email = input;
              },
              decoration:InputDecoration(
                contentPadding: EdgeInsets.only(top:5.0),
                border: InputBorder.none,
                hintStyle: TextStyle(fontSize: 18.0, color: Colors.grey,fontFamily: 'OpenSans'),
                icon: new Padding(
                    padding: EdgeInsets.fromLTRB(3, 10, 0,0),
                    child:Icon(
                      Icons.phone,size: 17,
                      color: Colors.grey,
                    )),
                hintText:"Mobile Number",
                counterText: ''

              ),//  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
            ))) ;

    nameField = Container(
        height: 50,
        width: MediaQuery.of(context).size.width/1.5,

        decoration: new BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.white,
        ),

        child:Padding (
            padding:const EdgeInsets.fromLTRB(23, 0,0, 0),
            child:TextFormField(

              textCapitalization: TextCapitalization.words,
              obscureText: false,
              controller: nameController,
              keyboardType: TextInputType.emailAddress,
              style: TextStyle(fontSize: 20),
              onSaved: (input) {
                // _email = input;
              },
              decoration:InputDecoration(
                contentPadding: EdgeInsets.only(top: 5.0),
                border: InputBorder.none,
                hintStyle: TextStyle(fontSize: 18.0, color: Colors.grey,fontFamily: 'OpenSans'),
                icon: new Padding(
                    padding: EdgeInsets.fromLTRB(3, 10, 0,0),
                    child:Icon(
                      Icons.person,size: 17,
                      color: Colors.grey,
                    )),
                hintText: "Name",

              ),//  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
            ))) ;


    checkbox = Container(
        height: 50,
        width: MediaQuery.of(context).size.width/1.5,

        decoration: new BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.white,
        ),

        child:Padding (
          padding:const EdgeInsets.fromLTRB(0, 0,0, 0),
          child:Padding (
            padding:const EdgeInsets.fromLTRB(0, 0,20, 0),
            child:CheckboxListTile(
              value: checkboxValue,
              onChanged: (val) {
                if (checkboxValue == false) {
                  setState(() {
                    checkboxValue = true;
                  });
                } else if (checkboxValue == true) {
                  setState(() {
                    checkboxValue = false;
                  });
                }
              },

              title: new Text(
                'Accept Terms And Conditions',
                style: TextStyle(fontSize: 15.0,color: Colors.red,fontFamily: 'OpenSans'),
              ),
              controlAffinity: ListTileControlAffinity.leading,
              activeColor: Colors.green,
            ),
          ),
        )) ;



    return Scaffold(

      appBar: EmptyAppBar(),
      body:GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        behavior: HitTestBehavior.translucent,
        child:  new Stack(
            children: <Widget>[
              new Container(
                decoration: new BoxDecoration(
                  image: new DecorationImage(image: new AssetImage("assets/images/background.png"), fit: BoxFit.cover,),
                ),
              ),
              SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.centerRight,
                      child:  Container(
                        margin: EdgeInsets.fromLTRB(0, 45, 0, 18),
                        padding: const EdgeInsets.only(left: 30.0,right: 0.0,top: 10.0,bottom: 10.0),
                        width: MediaQuery.of(context).size.width/3,
                        height: 50,
                        decoration: new BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey,
                              blurRadius: 23.0,
                            ),],
                          color:Colors.white,
                          borderRadius: new BorderRadius.only(
                            topLeft: const Radius.circular(200),
                            bottomLeft: const Radius.circular(200),
                          ),

                        ),

                        child:  new GestureDetector(
                            onTap: (){
                              Navigator.pop(context);

                            },
                            child:new Container(
                              child:Text("Login" ,textAlign: TextAlign.left, style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 23,fontFamily: 'OpenSans',color:AppColors.MAROON,)) ,)
                        ),

                      ),
                    ),
                    new Padding(
                        padding: EdgeInsets.fromLTRB(0, 0, 0,0),
                        child: new Container(child:Text("Register" , style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 30,fontFamily: 'OpenSans',color:AppColors.COLOR_BLUE)) ,)
                    ),

                    Container(
                      margin: const EdgeInsets.only(left: 0.0, top:30,right: 50.0),
                      width: MediaQuery.of(context).size.width,
                      height: 420,
                      decoration: new BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey,
                            blurRadius: 23.0,
                          ),],
                        color:Colors.white,
                        borderRadius: new BorderRadius.only(
                          topRight: const Radius.circular(100),
                          bottomRight: const Radius.circular(100),
                        ),
                      ),

                      child:Stack(
                        overflow: Overflow.visible,
                        children: <Widget>[
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [  //total container height is 150
                              SizedBox (height: 15.0),
                              nameField,
                              Padding(
                                padding:const EdgeInsets.fromLTRB(5, 0,120,0),
                                child:  Divider(
                                  color: Colors.grey,
                                  thickness: 1,
                                ),
                              ),
                              emailField, //height is 50
                              Padding (
                                padding:const EdgeInsets.fromLTRB(5, 0,120,0),
                                child:  Divider(
                                  color: Colors.grey,
                                  thickness: 1,
                                ),
                              ),
                              mobileField,//height is 50
                              Padding (
                                padding:const EdgeInsets.fromLTRB(5,0,120,0),
                                child:  Divider(
                                  color: Colors.grey,
                                  thickness: 1,
                                ),
                              ),
                              passwordField,
                              Padding (
                                padding:const EdgeInsets.fromLTRB(5,0,120,0),
                                child:  Divider(
                                  color: Colors.grey,
                                  thickness: 1,
                                ),
                              ),
                              confirmpasswordField,
                              Padding (
                                padding:const EdgeInsets.fromLTRB(5,0,120,0),
                                child:  Divider(
                                  color: Colors.grey,
                                  thickness: 1,
                                ),
                              ),
                              checkbox,

                            ],
                          ),
                          Positioned(
                            right: -35,top: 150,
                            child: Container(
                              margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                              child: new GestureDetector(
                                child:new Container(
                                  child:new Padding(
                                      padding: EdgeInsets.fromLTRB(15, 15, 15,15),
                                      child:Icon(
                                        Icons.check,size: 50,
                                        color: Colors.white,
                                      )) ,

                                  decoration: new BoxDecoration(

                                    image: new DecorationImage( image: new AssetImage("assets/images/loginbuttonbg.png",),),
                                  ),

                                ),
                                onTap: ()
                                {
                                  this.isValid();
                                },
                              ),
                            ),
                          )
                        ],
                      ),
                    ),



                    new Container(

                      margin: const EdgeInsets.only(bottom:20.0, right: 0.0,top: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [

                          Image.asset('assets/images/facebook.png',width: 60,height: 60,),

                        ],
                      ),
                    ),


                  ],
                ),
              ),







              _loadingState ?  getProgressBar() : Container()
            ]
        ),
      ),

    );


  }




}

class  EmptyAppBar  extends StatelessWidget implements PreferredSizeWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
  @override
  Size get preferredSize => Size(0.0,0.0);
}


