import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'dart:io' show Platform;


import 'package:crosswords/widgets/progress_bar.dart';
import 'package:crosswords/models/check_model.dart';


import 'package:crosswords/helpers/common_methods.dart';
import 'package:crosswords/helpers/app_colors.dart';
import 'package:crosswords/helpers/app_constants.dart';

import 'package:crosswords/helpers/app_constants.dart';
import 'dart:convert';


import 'package:crosswords/models/login_model.dart';
import 'package:crosswords/screens/verify_otp.dart';





class Login extends StatefulWidget {
  Login({Key key, this.title}) : super(key: key);

  static int k = -1;
  final String title;

  @override
  _MyLogInPageState createState() => new _MyLogInPageState();

}

class _MyLogInPageState extends State<Login> {



   _getToken() async{
    print("getting token--");
    var token = await commonMethod.getDeviceToken();
    print(token);

  }

  @override
  void initState()  {
    super.initState();
    _getToken();

  }


  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  var commonMethod = new CommonMethods();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

//  ProgressDialog  pr = new ProgressDialog(context);
//  pr.show();

  String deviceName,deviceVersion,identifier;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String _password,_email;
  bool isLoggedIn = false;


  bool _loadingState = false;

  void onLoginStatusChanged(bool isLoggedIn) {
    setState(() {
      this.isLoggedIn = isLoggedIn;
    });
  }

  void _login(username,password) async {
    _email = emailController.text;
    _password = passwordController.text;

    //dismiss keyboard
    FocusScope.of(context).requestFocus(FocusNode());


    String url = AppConstants.BASE_URL + 'auth/login';

    setState(() {
      _loadingState = true;
    });

    print(url);

    final response = await http.post(
        url,
        body: {
            'email':username,
            'password': password,
            'device_type':Platform.isAndroid ? '1' : '2',
            'device_token':await commonMethod.getDeviceToken()
      }
    );
      setState(() {
        _loadingState = false;
      });

    if (response.statusCode == 200) {
      var responseJson = jsonDecode(response.body);

      var loginResponse = LoginModel.fromJson(responseJson);
      print(loginResponse.status.toString());
      if(loginResponse.status) {
        commonMethod.showMessage(loginResponse.message);
      }else{
        commonMethod.showMessage(loginResponse.message,error: true);
      }

      var verifyStatus = loginResponse.result.status;
      if(verifyStatus == '0'){
        print("verifyStatus");
       // FocusScope.of(context).requestFocus(FocusNode());
        emailController.clear();
        passwordController.clear();
        //not verified //go to otp to verify account
        Navigator.push(context, new MaterialPageRoute(
          builder: (BuildContext context) => new VerifyOTP(email:loginResponse.result.email
              ,route:1),
        ));
      }
      else{
        print("verifyStatus no==");

        emailController.clear();
        passwordController.clear();

        //save all data and go to home
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString('user_id', loginResponse.result.id.toString());
        prefs.setString('name', loginResponse.result.first_name);
        prefs.setString('email', loginResponse.result.email);
        prefs.setString('mobile', loginResponse.result.mobile);
        prefs.setString('image', loginResponse.result.profile_pic);
        prefs.setString('jwt_token', loginResponse.result.jwt_token);

       // Navigator.pushNamed(context, '/home');
        Navigator.pushNamed(context, '/game');

      }

         print('loginResponse----: $responseJson');
    } else {
      var responseJson = jsonDecode(response.body);
      var loginResponse = CheckModel.fromJson(responseJson);
      commonMethod.showMessage(loginResponse.message,error: true);
      print('Request failed with status: ${response.body}.');
    }

    }




  void isValid(){
    _email = emailController.text;
    _password = passwordController.text;
    //dismiss keyboard
    FocusScope.of(context).requestFocus(FocusNode());
    if(emailController.text.isEmpty && passwordController.text.isEmpty)
    {
      commonMethod.showMessage("Please Enter Email Id and Password",error: true);
    }
    else if (emailController.text.isEmpty){
      commonMethod.showMessage("Enter Email",error: true);
    }
    else if(commonMethod.validateEmail (_email)!="Valid")
    {
      commonMethod.showMessage("Please Enter Valid Email ID",error: true);
    }
    else if (passwordController.text.isEmpty){
      commonMethod.showMessage("Please Enter Password",error: true);

    }  else if (passwordController.text.length<2 || passwordController.text.length>16 ) { // ||!(commonMethod.validatePassword(_password))
      commonMethod.showMessage("Please Enter Valid Password",error: true);
    }
    else {
      //commonMethod.showMessage("Login Successfully");
      _login(_email,_password);
      //login(_email,_password);
    }
  }





  @override
  Widget build(BuildContext context) {
    final emailField = Container(
        height: 50,
        width: MediaQuery.of(context).size.width/1.32,
        decoration: new BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.white,
        ),

        child:Padding (
            padding:const EdgeInsets.fromLTRB(10, 0,0, 0),
            child:TextFormField(
              obscureText: false,
              controller: emailController,
              keyboardType: TextInputType.emailAddress,
              style: TextStyle( fontSize: 20),
              onSaved: (input) {
                // _email = input;
              },
              decoration:InputDecoration(
                contentPadding: EdgeInsets.only(top: 5.0),
                border: InputBorder.none,
                hintStyle: TextStyle(fontSize: 15.0, color: Colors.grey,fontFamily: 'OpenSans'),
                icon: new Padding(
                    padding: EdgeInsets.fromLTRB(5, 10, 3,0),
                    child:Icon(
                      Icons.email,size: 17,
                      color: Colors.grey,
                    )),
                hintText: "EMAIL",

              ),//  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
            ))) ;
    final passwordField = Container(
        width: MediaQuery.of(context).size.width/1.32,
        padding:const EdgeInsets.fromLTRB(10, 0,0, 0),
        decoration: new BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.white,
        ),
        child: Padding (
            padding:const EdgeInsets.all(0.0),
            child: TextFormField(
              onFieldSubmitted: (_) =>this.isValid(),
              obscureText: true,
              controller: passwordController,
              style: style,
              validator: (String value){
                if (value.isEmpty){
                  return "Please enter the password";
                }
              },

                onSaved: (input) {
                //  _password = input;
              },
              decoration: InputDecoration(
                contentPadding: EdgeInsets.only(top: 5.0),
                border: InputBorder.none,
                hintStyle: TextStyle(fontSize: 15.0, color: Colors.grey,fontFamily: 'OpenSans'),
                icon: new Padding(
                    padding: EdgeInsets.fromLTRB(5, 10, 3,0),
                    child:Icon(
                      Icons.lock,size: 18,
                      color: Colors.grey,
                    )),
                hintText: "PASSWORD",
              ),

            )));

    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomPadding: false,
      appBar: EmptyAppBar(),
      body: new Stack(
          children: <Widget>[
            new Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(image: new AssetImage("assets/images/background.png"), fit: BoxFit.cover,),
              ),
            ),
            Positioned(
              top: MediaQuery.of(context).size.width/2.5,
              bottom: 0.0,
              left: MediaQuery.of(context).size.width/2.8,
              right: 0.0,
              child:new Padding(
                  padding: EdgeInsets.fromLTRB(0, 0, 0,0),
                  child:new Container(child:Text("Login" , style: TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 40,fontFamily: 'OpenSans',color:AppColors.COLOR_BLUE)) ,)
              ),
            ),

            new Center(
              child: Container(
                margin: const EdgeInsets.only( right: 25.0),
                width: MediaQuery.of(context).size.width,
                height: 150,
                decoration: new BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.blueAccent,
                      blurRadius: 23.0,
                    ),],
                  color:Colors.white,
                  borderRadius: new BorderRadius.only(
                    topRight: const Radius.circular(200),
                    bottomRight: const Radius.circular(200),
                  ),

                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [  //total container height is 150
                    SizedBox (height: 15.0),
                    emailField, //height is 50
                    Padding (
                      padding:const EdgeInsets.fromLTRB(5, 5,100,5),
                      child:  Divider(
                        color: Colors.grey,
                        thickness: 1,
                        height: 2,
                      ),
                    ),
                    passwordField,//height is 50
                  ],
                ),
              ),
            ),
            new Center(
                child:Align(
                  alignment:Alignment.centerRight,
                    child:  new GestureDetector(
                    child: new Container(
                    width: 80,
                    height: 80,
                    child:new Padding(
                        padding: EdgeInsets.fromLTRB(5, 5, 5,5),
                        child:Icon(
                          Icons.arrow_forward,size: 50,
                          color: Colors.white,
                        )) ,
                       decoration: new BoxDecoration(
                      image: new DecorationImage( image: new AssetImage("assets/images/loginbuttonbg.png",), fit: BoxFit.fill,),

                    ),
                  ),
                    onTap: ()
                    {
                       //Navigator.pushNamed(context, '/home');
                      isValid();

                    },
                  ),
                )

            ),
            new Center(
              child:  new GestureDetector(
                onTap: (){

                  Navigator.pushNamed(context, '/forgot_password');
                },
                child:Container(
                    margin: const EdgeInsets.only( top:200.0, right: 30.0),
                    alignment:  Alignment.centerRight,
                    child: new Container(child:Text("Forgot Password ?" , style: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 20,fontFamily: 'OpenSans',color:Colors.grey)) ,)
                ),
              ),
            ),
            new Center(
              child: Container(
                margin: const EdgeInsets.only( top:300.0, right: 0.0),
                alignment:  Alignment.centerLeft,
                child:Container(
                  padding: const EdgeInsets.only(left: 10.0,right: 20.0,top: 10.0,bottom: 10.0),
                  width: MediaQuery.of(context).size.width/3,
                  height: 50,
                  decoration: new BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey,
                        blurRadius: 23.0,
                      ),],
                    color:Colors.white,
                    borderRadius: new BorderRadius.only(
                      topRight: const Radius.circular(200),
                      bottomRight: const Radius.circular(200),
                    ),

                  ),

                  child:  new GestureDetector(
                      onTap: (){
                        emailController.clear();
                        passwordController.clear();
                        Navigator.pushNamed(context, '/register');

                      },
                      child:new Container(
                        child:Text("Register" ,textAlign: TextAlign.right,
                            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 23,fontFamily: 'OpenSans',color:AppColors.MAROON)
                        ) ,)
                  ),

                ),     ),
            ),
            new Container(
              alignment: Alignment.bottomCenter,
              margin: const EdgeInsets.only( bottom:60.0, right: 0.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [

                   Image.asset('assets/images/facebook.png',width: 50,height: 50,),
                ],
              ),
            ),


           _loadingState ?  getProgressBar() : Container()
          ]
      ),
    );
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value,style: TextStyle(color: Colors.white, fontSize: 15.0),),
      backgroundColor: Colors.red,
    ));
  }

}
class  EmptyAppBar  extends StatelessWidget implements PreferredSizeWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
  @override
  Size get preferredSize => Size(0.0,0.0);
}

/**
 *  Navigator.push(context, new MaterialPageRoute(
    builder: (BuildContext context) => new Advise(title:category_list[position].name,cat_id:category_list[position].cat_id),
    ));
 */

//REcievising side

/**
 *  Advise({Key key, this.title,this.cat_id}) : super(key: key);


    final String title;
    final int cat_id;
 *
 * getTitle(){
    return widget.title;
    }
    getId(){
    return widget.cat_id;
    }
 */



