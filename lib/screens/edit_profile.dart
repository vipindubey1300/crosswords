import 'package:crosswords/helpers/app_colors.dart';
import 'package:crosswords/models/login_model.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'dart:io' show Platform;
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'package:crosswords/widgets/progress_bar.dart';
import 'package:dio/dio.dart';

import 'dart:typed_data';
import 'package:crosswords/helpers/app_constants.dart';

import 'package:crosswords/helpers/common_methods.dart';
import 'package:crosswords/screens/login.dart';


class EditProfile extends StatefulWidget {

  EditProfile({Key key, this.title}) : super(key: key);

  final String title;
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _EditProfileState();
  }
}

class _EditProfileState extends State<EditProfile> {

  var commonMethod = new CommonMethods();
  bool editable=false;
  TextEditingController  nameController,emailController,  mobileNumberController,addressController;

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String _emailId,_name,_mobileNumber,_userAddress,_userImage;
  File _image;
  bool _loadingState = false;

  Future getProfile() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('jwt_token');
    String url = AppConstants.BASE_URL + 'dashboard/profile?auth=' + token;
    setState(() {_loadingState = true;});

    final response = await http.get(url);
    setState(() {_loadingState = false;});
    try {
      var responseJson = jsonDecode(response.body);
      print(responseJson);
      if(responseJson['status']){
        var name = responseJson['result']['first_name'];
        var image = responseJson['result']['profile_pic'];
        var mobile = responseJson['result']['mobile'];
        var email = responseJson['result']['email'];

        _name =  name;
        _mobileNumber = mobile;
        _emailId = email;
        _userImage = image;

        setState(() {

          nameController = TextEditingController(text: _name);
          emailController = TextEditingController(text: _emailId);
          mobileNumberController = TextEditingController(text: _mobileNumber);

        });

      }

    } catch (e) {
      print(e.toString());
      commonMethod.showMessage('Error in fetching profile.. ',error: true);
    }
  }


  Future<Null> getSharedPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _name = prefs.getString("name");
    _mobileNumber= prefs.getString("mobile");
    _emailId= prefs.getString("email");
    _userImage =prefs.getString("image");

    setState(() {

      nameController = TextEditingController(text: _name);
      emailController = TextEditingController(text: _emailId);
      mobileNumberController = TextEditingController(text: _mobileNumber);
      addressController = TextEditingController(text: _userAddress);
    });
  }
  @override
  void initState() {
    super.initState();
    _name = "";
    _userAddress="";
    _emailId="";
    _mobileNumber="";
    _userImage='';
   // getSharedPrefs();
    getProfile();


  }

  void _upload(File file,name,mobile) async {
    print("IN DIO");
    setState(() {
      _loadingState = true;
    });

    String fileName =  file == null ? '' :  file.path.split('/').last;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('jwt_token');

    FormData data = FormData.fromMap({
      "profile_pic": file == null ? '' :  await MultipartFile.fromFile(file.path, filename: fileName,),
      'name':name,
      'mobile':mobile,
      'auth':token,

    });
    print(data.fields);

    Dio dio = new Dio();
   // dio.options.headers['content-Type'] = 'application/json';


    String url = AppConstants.BASE_URL + 'dashboard/profile';

    try{
      var response = await  dio.post(url, data: data);
      var responseBody = response.data;
      print(responseBody.toString());
      setState(() {
        _loadingState = false;
      });

      if(responseBody['status']){
       commonMethod.showMessage('Profile updated successfully');

        var name = responseBody['result']['first_name'];
        var image = responseBody['result']['profile_pic'];
        var mobile = responseBody['result']['mobile'];

        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString('name', name);
        prefs.setString('mobile',mobile);
        prefs.setString('image',image);

        Navigator.pushNamed(context, '/home');
      }
      else{
        commonMethod.showMessage(responseBody['message'],error: true);
      }
      //save all data and go to home

    }
    catch(e){
      setState(() {
        _loadingState = false;
      });
      print(e.toString());
      commonMethod.showMessage('Try Again .. ',error: true);
    }



//
//    dio.post(url, data: data)
//        .then((response) => {
//            print( response.data);
//        })
//        .catchError((error) => print(error.toString()));
  }


  void _editProfile(name,mobile) async {
    print("IN NoRMAL");

    setState(() {_loadingState = true;});

    String url = AppConstants.BASE_URL + 'dashboard/profile';
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('jwt_token');

    var body =  {'name':name,
      'mobile':mobile,
      'auth':token,
    };

    print(body);
    final response = await http.post(url,body:body);
    print(response.body);
    setState(() {_loadingState = false;});

    try {
      var responseJson = jsonDecode(response.body);
      print(response.body);
      var res = LoginModel.fromJson(responseJson);
     // print(response.body);

      if(res.status) {
        commonMethod.showMessage(res.message);

        SharedPreferences prefs = await SharedPreferences.getInstance();

        prefs.setString('name', res.result.first_name);
        prefs.setString('mobile', res.result.mobile);
        Navigator.pushNamed(context, '/home');

      }else{
        commonMethod.showMessage(res.message,error: true);
      }

    } catch (e) {
      commonMethod.showMessage(e.toString(),error: true);
    }
  }

  void isValid(){
    var name = nameController.text;
    var mobile = mobileNumberController.text;

    if (name.isEmpty){
      commonMethod.showMessage("Enter Name",error: true);
    }
    else  if (mobile.isEmpty){
      commonMethod.showMessage("Enter Mobile Number",error: true);
    }
    else if (mobile.length < 10){
      commonMethod.showMessage("Please Enter Valid Mobile Number",error: true);
    }
    else {
    // _image == null ? _editProfile(name, mobile) :  _upload(_image, name, mobile);
    // _editProfile(name, mobile);
      _upload(_image, name, mobile);
    }
  }


  @override
  Widget build(BuildContext context) {
    ProgressDialog pr;
    pr = new ProgressDialog(context);
    final focus = FocusNode();
    final emailField = Container(
        height: 43,
        width: MediaQuery.of(context).size.width/1.32,
        decoration: new BoxDecoration(

          shape: BoxShape.rectangle,
          color: Colors.transparent,
        ),

        child:Padding (
            padding:const EdgeInsets.fromLTRB(10, 0,0, 0),
            child:TextFormField(
              focusNode: focus,
              obscureText: false,
              enabled: false,
              controller: emailController,
              keyboardType: TextInputType.emailAddress,
              style: TextStyle( fontSize: 20,color: Colors.grey),

              onSaved: (input) {
                // _email = input;
              },
              decoration:InputDecoration(
                alignLabelWithHint: true,
                contentPadding: EdgeInsets.all(10),
                border: InputBorder.none,
                hintStyle: TextStyle(fontSize: 15.0, color: Colors.grey,fontFamily: 'OpenSans'),
                hintText: "Enter Email",

              ),//  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
            ))) ;
    final nameField = Container(
        height: 41,
        width: MediaQuery.of(context).size.width/1.32,
        decoration: new BoxDecoration(

          color: Colors.transparent,
        ),

        child:Padding (
            padding:const EdgeInsets.fromLTRB(10, 0,0, 0),
            child:TextFormField(
              obscureText: false,
              enabled: (editable==false)?false:true,
              controller: nameController,
              keyboardType: TextInputType.text,
              style: TextStyle( fontSize: 18,color: editable==false? Colors.grey:Colors.white),
              onSaved: (input) {
                // _email = input;
              },
              decoration:InputDecoration(
                contentPadding: EdgeInsets.all(10),
                border: InputBorder.none,
                hintStyle: TextStyle(fontSize: 18.0, color:Colors.grey,fontFamily: 'OpenSans'),
                hintText: "Enter Name",

              ),//  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
            ))) ;
    final mobileField = Container(
        height: 40,
        width: MediaQuery.of(context).size.width/1.32,
        decoration: new BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.transparent,
        ),

        child:Padding (
            padding:const EdgeInsets.fromLTRB(10, 0,0, 0),
            child:TextFormField(
              enabled: (editable==false)?false:true,
              controller: mobileNumberController,
              keyboardType: TextInputType.phone,
              maxLength: 11,
              style: TextStyle( fontSize: 20,color:editable==false? Colors.grey:Colors.white),
              onSaved: (input) {
                // _email = input;
              },
              decoration:InputDecoration(
                contentPadding: EdgeInsets.all(10),
                border: InputBorder.none,
                hintStyle: TextStyle(fontSize: 18.0, color:Colors.grey,fontFamily: 'OpenSans'),
                hintText:"Enter Contact",
                counterText: "",//to remove 0/6 marking in text inpuy
              ),//  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
            ))) ;
    final addressField = Container(
        height: 43,
        width: MediaQuery.of(context).size.width/1.32,
        decoration: new BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.transparent,
        ),

        child:Padding (
            padding:const EdgeInsets.fromLTRB(10, 0,0, 0),
            child:TextFormField(
              enabled: (editable==false)?false:true,
              controller: addressController,
              keyboardType: TextInputType.multiline,
              style: TextStyle( fontSize: 20,color: editable==false? Colors.grey:Colors.black),
              onSaved: (input) {
                // _email = input;
              },
              decoration:InputDecoration(
                contentPadding: EdgeInsets.all(10),
                border: InputBorder.none,

                hintStyle: TextStyle(fontSize: 18.0, color:Colors.grey,fontFamily: 'OpenSans'),
                hintText:"Enter Address",

              ),//  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
            ))) ;
    final loginButon = GestureDetector(
      child: new Container(
        margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
        width: 180,
        height: 50,
        child:new Padding(
          padding: EdgeInsets.fromLTRB(5, 5, 5,5),
          child:Center(child: Text(
            'Save Change',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontSize: 19, fontFamily: 'Rock Salt',fontWeight: FontWeight.bold,

            ),
          ),),
        ) ,
        decoration: new BoxDecoration(
          borderRadius:BorderRadius.circular(15.0),
          image: new DecorationImage(
            image: new AssetImage("assets/images/buttongradient.png",), fit: BoxFit.fill,
          ),

        ),
      ),
      onTap: ()
      {
       this.isValid();

      },
    );

    return Scaffold(
      key: _scaffoldKey,
     backgroundColor: AppColors.PRIMARY_COLOR,
      appBar: GradientAppBar(
        centerTitle: true,
        title: Text(editable ? 'Edit Profile' : 'Profile'),
        gradient: LinearGradient(
            colors: [AppColors.PRIMARY_COLOR, AppColors.PRIMARY_COLOR, Colors.black]),
        actions: <Widget>[
          new SizedBox(
            child: IconButton(
              padding: new EdgeInsets.all(10.0),
              icon: editable == false  ? Icon(Icons.edit, size: 17,) :  Icon(Icons.clear, size: 17,),
              onPressed: () {
                setState(() {
                  editable=!editable;
                });
                if(editable) FocusScope.of(context).requestFocus(focus);;
                //  _handlePressButton();
              },
            ),
          ),
        ],
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        behavior: HitTestBehavior.translucent,
        child: new Stack(
            children: <Widget>[
              new Container(
                decoration: new BoxDecoration(
                  image: new DecorationImage(image: new AssetImage("assets/images/forgotPassword.png"), fit: BoxFit.cover,),
                ),
              ),

              SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Container(
                        margin: const EdgeInsets.only(top: 30.0,left: 10,right: 10,bottom: 10),
                        width: MediaQuery.of(context).size.width,
                        decoration: new BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black,
                              blurRadius: 30.0, // has the effect of softening the shadow
                              spreadRadius: 2.0, // has the effect of extending the shadow
                              offset: Offset(
                                10.0, // horizontal, move right 10
                                10.0, // vertical, move down 10
                              ),
                            ),],
                          color: AppColors.PRIMARY_COLOR,
                          borderRadius: new BorderRadius.circular(10.0),
                        ),
                        child:Padding(
                          padding: EdgeInsets.all(20.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Align(
                                alignment: Alignment.center,
                                child:  Container(
                                  height: 120,width:120,
                                  child: Stack(
                                    children: <Widget>[
                                      _image == null
                                          ?
                                      CircleAvatar(backgroundColor: Colors.white,radius: 100,
                                        backgroundImage: NetworkImage(AppConstants.BASE_URL_IMAGE + _userImage),)
                                          :
                                      ClipRRect(
                                        borderRadius: BorderRadius.circular(60.0),
                                        child:  Image.file(_image,width: 120,height: 120,fit: BoxFit.cover,),
                                      ),

                                      Positioned(

                                        bottom: 2,right: 14,
                                        child: InkWell(

                                          onTap: (){

                                            editable == false ? null : _onImageEdit();
                                          },
                                          child:CircleAvatar(backgroundColor: Colors.white,radius: 15,
                                            backgroundImage: AssetImage("assets/images/image-edit.png"),),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),


                              SizedBox (height: 10.0),
                              new Padding(
                                padding:const EdgeInsets.fromLTRB(22, 10,0, 0),

                                child:Text("Email " ,style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 12,fontFamily: 'OpenSans',color:Colors.grey)),


                              ),
                              emailField, //height is 50
                              Padding (
                                padding:const EdgeInsets.fromLTRB(15, 0,15, 0),
                                child:Divider(
                                  color: Colors.grey,
                                  thickness: 0.6,
                                  height: 1,
                                ),
                              ),
                              SizedBox (height: 9.0),
                              new Padding(
                                padding:const EdgeInsets.fromLTRB(22, 5,0, 0),

                                child:Text("Name " ,style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 12,fontFamily: 'OpenSans',color:Colors.grey)),


                              ),
                              nameField,
                              Padding (
                                padding:const EdgeInsets.fromLTRB(15, 0,15, 0),
                                child:Divider(
                                  color: Colors.grey,
                                  thickness: 0.6,
                                  height: 1,
                                ),
                              ),
                              SizedBox (height: 9.0),
                              new Padding(
                                padding:const EdgeInsets.fromLTRB(22, 5,0, 0),

                                child:Text("Number " ,style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 12,fontFamily: 'OpenSans',color:Colors.grey)),


                              ),
                              mobileField,
                              Padding (
                                padding:const EdgeInsets.fromLTRB(15, 0,15, 0),
                                child:Divider(
                                  color: Colors.grey,
                                  thickness: 0.6,
                                  height: 1,
                                ),
                              ),
                              SizedBox (height: 6.0),


                            ],
                          ),
                        )
                    ),
                    Center(
                      child:Container(
                        margin: const EdgeInsets.fromLTRB(0,30,0,0),
                        width: MediaQuery.of(context).size.width/2,
                        height: 50,
                        decoration: new BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color:Colors.transparent,
                            ),],
                          color:Colors.transparent,
                          borderRadius: new BorderRadius.circular(2.0),
                        ),
                        child: editable == false ? null : loginButon,
                      ),
                    ),
                    // Image.memory(base64Decode("iVBORw0KGgoAAAANSUhEUgAAAJYAAACWCAIAAACzY+a1AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6QTg0QUFENjM1MTU4MTFFQTlBQjk5RDc5NzFGQkYyNTciIHhtcE1NOkRvY3VtZW50SUQ9Inhtc"),height: 200,width: 200,),
                    //Image.memory(base64Decode('asds'))


                  ],
                ),
              ),

              _loadingState ?  getProgressBar() : Container()






            ]
        ),
      ),
    );
  }

  Future<bool> _onImageEdit() async{
    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Image Pick'),
        content: new Text('Choose Image Source'),
        actions: <Widget>[
          new GestureDetector(
            onTap: () async {
              Navigator.pop(context); //close dialog
              var image = await ImagePicker.pickImage(source: ImageSource.gallery);
              setState(() {
                _image = image;
              });

            },
            child:    new Container(
                padding: new EdgeInsets.fromLTRB(0, 16, 20, 10),
                child: Text("GALLERY",style: TextStyle(color: Colors.blue),),
            ),
          ),
          SizedBox(height: 16),
          new GestureDetector(
            onTap: () async{
              Navigator.pop(context);  //close dialog
              var image = await ImagePicker.pickImage(source: ImageSource.camera);
              setState(() {
                _image = image;
              });

            },
            child:   new Container(
                padding: new EdgeInsets.fromLTRB(0, 16, 20, 10),
                child: Text("CAMERA",style: TextStyle(color: Colors.blue),),
            ),
          ),
        ],
      ),
    ) ??
        false;
  }



  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value,style: TextStyle(color: Colors.white, fontSize: 15.0),),
      backgroundColor: Colors.red,
    ));
  }

}
class  EmptyAppBar  extends StatelessWidget implements PreferredSizeWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
  @override
  Size get preferredSize => Size(0.0,0.0);
}