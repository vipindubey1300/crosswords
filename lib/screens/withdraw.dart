import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'dart:io' show Platform;
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

import 'package:crosswords/helpers/app_colors.dart';

import 'package:crosswords/helpers/common_methods.dart';
import 'package:crosswords/screens/login.dart';


class Withdraw extends StatefulWidget {

  Withdraw({Key key, this.title}) : super(key: key);

  final String title;
  @override
  _WithdrawState createState() => new _WithdrawState();
}

class _WithdrawState extends State<Withdraw> {

  var commonMethod = new CommonMethods();


  final _scaffoldKey = GlobalKey<ScaffoldState>();


  @override
  Widget build(BuildContext context) {

    final amountEdit = Container(
        height: 50,

        decoration: new BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.transparent,
        ),
        child:Padding (
            padding:const EdgeInsets.fromLTRB(5, 0,0, 0),
            child:TextFormField(
              style: TextStyle( fontSize: 20,color: Colors.grey),
              onSaved: (input) {
                // _email = input;
              },
              decoration:InputDecoration(
                contentPadding: EdgeInsets.all(10),
                border: InputBorder.none,
                hintStyle: TextStyle(fontSize: 16.0, color: Colors.grey,fontFamily: 'OpenSans'),
                hintText: "Amount",
              ),//  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
            ))) ;

    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomPadding: false,
      appBar: GradientAppBar(
        centerTitle: true,
        title: Text('Withdraw'),
        gradient: LinearGradient(
            colors: [AppColors.PRIMARY_COLOR, AppColors.PRIMARY_COLOR, Colors.black]),
      ),
      body: new Stack(
          children: <Widget>[
            new Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(image: new AssetImage("assets/images/forgotPassword.png"), fit: BoxFit.cover,),
              ),
            ),
            SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Container(
                      margin: const EdgeInsets.only(top: 30.0,left: 10,right: 10,bottom: 10),
                      width: MediaQuery.of(context).size.width,
                      decoration: new BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black,
                            blurRadius: 30.0, // has the effect of softening the shadow
                            spreadRadius: 2.0, // has the effect of extending the shadow
                            offset: Offset(
                              10.0, // horizontal, move right 10
                              10.0, // vertical, move down 10
                            ),
                          ),],
                        color: AppColors.PRIMARY_COLOR,
                        borderRadius: new BorderRadius.circular(10.0),
                      ),
                      child:Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Expanded(
                              child: Container(
                                child:  Image.asset('assets/images/bitcoin.png',width: 40,height: 40,),
                              ),
                              flex: 2,
                            ),

                            new Expanded(
                              child: Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text('Total wallet balance',style: TextStyle(color: Colors.white,fontSize: 17,fontWeight: FontWeight.bold),),
                                        Text('COINS',style: TextStyle(color: Colors.grey),)
                                      ],
                                    ),
                                    SizedBox(height: 17,),
                                    Text('23.32423 COINS',style: TextStyle(color: Colors.white,fontSize: 27,fontWeight: FontWeight.w500),),
                                    SizedBox(height: 5,),
                                    Text("19.2 USD ",style: TextStyle(color: Colors.white,fontSize: 14),),



                                  ],
                                ),
                              ),
                              flex: 9,
                            ),
                          ],
                        ),
                      )
                  ),
                  Container(
                      margin: const EdgeInsets.only(top: 30.0,left: 10,right: 10,bottom: 10),
                      width: MediaQuery.of(context).size.width,
                      decoration: new BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black,
                            blurRadius: 30.0, // has the effect of softening the shadow
                            spreadRadius: 2.0, // has the effect of extending the shadow
                            offset: Offset(
                              10.0, // horizontal, move right 10
                              10.0, // vertical, move down 10
                            ),
                          ),],
                        color: AppColors.PRIMARY_COLOR,
                        borderRadius: new BorderRadius.circular(10.0),
                      ),
                      child:Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Column(
                            children: <Widget>[
                              Text('Withdraw',style: TextStyle(color: Colors.white,fontSize: 17,fontWeight: FontWeight.w800),),

                              SizedBox (height: 8.0),
                              amountEdit, //height is 50
                              Padding (
                                padding:const EdgeInsets.fromLTRB(15, 0,15, 0),
                                child:Divider(
                                  color: Colors.grey,
                                  thickness: 0.6,
                                ),
                              ),


                            ],
                          )
                      )
                  ),
                  SizedBox(height: 30.0,),
                  Center(
                    child: new GestureDetector(
                      child: new Container(
                        margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        width: 220,
                        height: 50,
                        child:new Padding(
                          padding: EdgeInsets.fromLTRB(5, 5, 5,5),
                          child:Center(child: Text(
                            'Send',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 19, fontFamily: 'Rock Salt',fontWeight: FontWeight.w100,

                            ),
                          ),),
                        ) ,
                        decoration: new BoxDecoration(
                          borderRadius:BorderRadius.circular(15.0),
                          image: new DecorationImage(
                            image: new AssetImage("assets/images/buttongradient.png",), fit: BoxFit.fill,
                          ),

                        ),
                      ),
                      onTap: ()
                      {
                        Navigator.pushNamed(context, '/verify_otp');

                      },
                    ),
                  ),
                  SizedBox(height: 15.0,),
                  Center(
                    child: new GestureDetector(
                      child: new Container(
                        margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        width: 220,
                        height: 50,
                        child:new Padding(
                          padding: EdgeInsets.fromLTRB(5, 5, 5,5),
                          child:Center(child: Text(
                            'Cancel',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 19, fontFamily: 'Rock Salt',fontWeight: FontWeight.w100,

                            ),
                          ),),
                        ) ,
                        decoration: new BoxDecoration(
                          borderRadius:BorderRadius.circular(15.0),
                          image: new DecorationImage(
                            image: new AssetImage("assets/images/buttongradient.png",), fit: BoxFit.fill,
                          ),

                        ),
                      ),
                      onTap: ()
                      {
                        Navigator.pushNamed(context, '/verify_otp');

                      },
                    ),
                  ),


                ],
              ),
            ),



          ]
      ),
    );
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value,style: TextStyle(color: Colors.white, fontSize: 15.0),),
      backgroundColor: Colors.red,
    ));
  }

}
class  EmptyAppBar  extends StatelessWidget implements PreferredSizeWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
  @override
  Size get preferredSize => Size(0.0,0.0);
}