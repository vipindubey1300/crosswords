import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'dart:io' show Platform;
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:crosswords/helpers/app_constants.dart';
import 'package:crosswords/helpers/app_colors.dart';
import 'package:crosswords/helpers/common_methods.dart';
import 'package:crosswords/screens/login.dart';
import 'package:crosswords/widgets/word_marker.dart';

class Game extends StatefulWidget {

  Game({Key key, this.title}) : super(key: key);

  final String title;
  @override
  _GameState createState() => new _GameState();
}

class _GameState extends State<Game> {

  var commonMethod = new CommonMethods();
  final _scaffoldKey = GlobalKey<ScaffoldState>();


  final int wordsPerLine = 11;
  final List<String> alphabet = ['I', 'A', 'G', 'M', 'F', 'Y', 'L', 'I', 'R', 'V', 'P', 'D',
    'B', 'R', 'A', 'I', 'N', 'S', 'T', 'O', 'R', 'M', 'E', 'S', 'S', 'T', 'R', 'A', 'T', 'E', 'G', 'Y',
    'E', 'A', 'B', 'W', 'O', 'M', 'G', 'O', 'A', 'L', 'S', 'X', 'S', 'Q', 'U', 'K', 'H', 'J', 'P', 'M', 'D', 'W', 'S'
  ];
  final List<String> words = ['ARTHER', 'GOLDEN', 'AMADEUS', 'IDEAS', 'GOALS', 'BRAINSTORM'];



  final markers = <WordMarker>[];
  int correctAnswers = 0;
  var uniqueLetters;




  List<bool> isSelected = [];
  List<String> selectedLetters = [];
  Map<GlobalKey, String> lettersMap;

  Offset initialTappedPosition = Offset(0, 0);
  Offset initialPosition = Offset(0, 0);
  Offset finalPosition;

  int intialSquare;
  int crossAxisCount = 4; //whether you use GridView or not still need to provide this
  int index = -1;
  bool isTapped = false;

  String selectedWord = '';

  double width = 50;
  double height = 50;
  Size size;

  List<String> letters = [
    'a',
    'b',
    'c',
    'd',
    'e',
    'f',
    'g',
    'h',
    'i',
    'j',
    'k',
    'b',
    'b',
    'b',
    'b',
    'z',
  ];

  @override
  void initState() {
    super.initState();
    lettersMap = Map.fromIterable(letters, key: (i) => GlobalKey(), value: (i) => i[0]);
    isSelected = List.generate(letters.length, (e) => false);
    uniqueLetters = alphabet.map((letter) => {'letter': letter, 'key': GlobalKey()}).toList();

  }

  _determineWord() {
    double difference;
    int numberOfSquares;

    if ((finalPosition.dx - initialPosition.dx) > 20) {
      print('right');

      //moved right
      difference = finalPosition.dx - initialPosition.dx;
      numberOfSquares = (difference / size.width).ceil();
      for (int i = intialSquare + 1;
      i < (intialSquare + numberOfSquares);
      i++) {
        isSelected[i] = true;
      }
      for (int i = 0; i < isSelected.length; i++) {
        if (isSelected[i]) {
          selectedWord += letters[i];
        }
      }
      print(selectedWord);
    } else if ((initialPosition.dx - finalPosition.dx) > 20) {
      print('left');

      // moved left
      difference = finalPosition.dx + initialPosition.dx;
      numberOfSquares = (difference / size.width).ceil();
      for (int i = intialSquare - 1; i >= (intialSquare - numberOfSquares + 1); i--) {
        isSelected[i] = true;
      }
      for (int i = 0; i < isSelected.length; i++) {
        if (isSelected[i]) {
          selectedWord += letters[i];
        }
      }
      print(selectedWord);
    } else if ((finalPosition.dy - initialPosition.dy) > 20) {
      //moved up when moving up/down number of squares numberOfSquares is also number of rows

      difference = finalPosition.dy - initialPosition.dy;
      numberOfSquares = (difference / size.height).ceil();

      for (int i = intialSquare + crossAxisCount;
      i < (intialSquare + (numberOfSquares * crossAxisCount));
      i += 4) {
        isSelected[i] = true;
      }
      for (int i = 0; i < isSelected.length; i++) {
        if (isSelected[i]) {
          selectedWord += letters[i];
        }
      }

      print(selectedWord);
    } else if ((initialPosition.dy - finalPosition.dy) > 20) {
      //moved down
      difference = initialPosition.dy - finalPosition.dy;
      numberOfSquares = (difference / size.height).ceil();

      for (int i = intialSquare - crossAxisCount;
      i > (intialSquare - (numberOfSquares * crossAxisCount));
      i -= 4) {
        isSelected[i] = true;
        print('$i');
      }
      for (int i = isSelected.length - 1; i >= 0; i--) {
        if (isSelected[i]) {
          selectedWord += letters[i];
        }
      }
      print(selectedWord);
    }
  }



  void addMarker(Rect rect, int startIndex) {
    markers.add(WordMarker(
      rect: rect,
      startIndex: startIndex,
    ));
  }

  WordMarker adjustedMarker(WordMarker originalMarker, Rect endRect) {
    return originalMarker.copyWith(
        rect: originalMarker.rect.expandToInclude(endRect));
  }

  String pathAsString(int start, int end) {
// a = a+ 5 is eqaul to a += 5
//~/ return divison (int)
// % return remainder
    final isHorizontal = start / wordsPerLine == end / wordsPerLine;
    final isVertical = start % wordsPerLine == end % wordsPerLine;

    String result = '';

    if (isHorizontal) {
      result = alphabet.sublist(start, end + 1).join();
    } else if (isVertical) {
      for (int i = start;
      i < alphabet.length;
      i += wordsPerLine) {
        result += alphabet[i];
      }
    }

    return result;
  }

//var words = ['Ronaldo','Eminem','Messi','Pitbull','Neymar'];

//var letters =["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomPadding: false,
      appBar: GradientAppBar(
        centerTitle: true,
        title: Text('Crosswords'),
        gradient: LinearGradient(
            colors: [AppColors.PRIMARY_COLOR, AppColors.PRIMARY_COLOR, Colors.black]
        ),
        actions: <Widget>[
          new SizedBox(
            child: IconButton(
              padding: new EdgeInsets.all(8.0),
              icon: Icon(Icons.account_balance_wallet, size: 20,),
              onPressed: () {
              },
            ),
          ),
        ],
      ),
      body: new Stack(
          children: <Widget>[
            new Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(image: new AssetImage("assets/images/forgotPassword.png"), fit: BoxFit.cover,),
              ),
            ),
            SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          new SizedBox(
                            child: IconButton(
                              icon: Icon(Icons.access_time, size: 20,color: Colors.white,),
                              onPressed: () {
                                Navigator.pushNamed(context, '/game');
                              },
                            ),
                          ),
                          new Container(
                              child: Text("04:10:45" ,textAlign: TextAlign.right, style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 18,fontFamily: 'OpenSans',color:Colors.white,))
                          ),
                        ],
                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(0, 10, 10, 0),

                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(8.0),
                          child: Image.asset(
                            'assets/images/main.jpg',
                            width: 95.0,
                            height: 95.0,
                            fit: BoxFit.fill,
                          ),
                        ),
                      )

                    ],
                  ),
                  SizedBox(height: 20,),
                  Align(
                      alignment: Alignment.center,
                      child: Text('Word Match',style: TextStyle(color: Colors.white,fontSize: 22,fontWeight: FontWeight.bold),)
                  ),

                  Container(
                    margin: const EdgeInsets.only(top: 30.0,left: 10,right: 10,bottom: 10),
                    width: MediaQuery.of(context).size.width,
                    decoration: new BoxDecoration(
                      color: AppColors.PRIMARY_COLOR_TRANSPARENT,
                      borderRadius: new BorderRadius.circular(10.0),
                      border: Border.all(
                        color: Colors.yellow, // <--- border color
                        width: 2.0,
                      ),
                    ),
                    child: GridView(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount:3,
                          childAspectRatio: 3
                      ),
                      children: <Widget>[
                        ...words.map((letter) =>
                            GestureDetector(
                                behavior: HitTestBehavior.opaque,
                                child: Padding(
                                  padding: EdgeInsets.only(top:10),
                                  child: Text(
                                    letter.toUpperCase(),
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize:18,
                                        fontWeight: FontWeight.w300
                                    ),
                                  ),
                                )

                            )
                        ).toList(),
                      ],
                    ),
                  ),
                  Container(
                      margin: const EdgeInsets.only(top: 10.0,left: 10,right: 10,bottom: 10),
                      width: MediaQuery.of(context).size.width,
                      decoration: new BoxDecoration(
                        color: AppColors.PRIMARY_COLOR_TRANSPARENT,
                        borderRadius: new BorderRadius.circular(10.0),
                      ),
                      child:Padding(
                          padding: EdgeInsets.all(0.0),
                          child: Stack(
                            children: <Widget>[
                              Center(
                                child: Padding(
                                  padding: const EdgeInsets.all(30.0),
                                  child: GestureDetector(
                                    child: GridView(
                                      physics: NeverScrollableScrollPhysics(), //Very Important if
                                      // you don't have this line you will have conflicting touch inputs and with
                                      // gridview being the child will win
                                      shrinkWrap: true,
                                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                        crossAxisCount: crossAxisCount,
                                        childAspectRatio: 2,
                                      ),
                                      children: <Widget>[
                                        for (int i = 0; i != lettersMap.length; ++i)
                                          Listener(
                                            child: Container(
                                              key: lettersMap.keys.toList()[i],
                                              child: Text(
                                                lettersMap.values.toList()[i],
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                  color: Colors.amber,
                                                  fontSize: 18,
                                                ),
                                              ),
                                            ),
                                            onPointerDown: (PointerDownEvent event) {
                                              final RenderBox renderBox = lettersMap.keys
                                                  .toList()[i]
                                                  .currentContext
                                                  .findRenderObject();
                                              size = renderBox.size;
                                              setState(() {
                                                isSelected[i] = true;
                                                intialSquare = i;
                                              });
                                            },
                                          ),
                                      ],
                                    ),
                                    onTapDown: (TapDownDetails details) {
                                      //User Taps Screen
                                      // print('Global Position: ${details.globalPosition}');
                                      setState(() {
                                        initialPosition = Offset(
                                          details.globalPosition.dx - 25,
                                          details.globalPosition.dy - 25,
                                        );
                                        initialTappedPosition = Offset(
                                          details.globalPosition.dx - 25,
                                          details.globalPosition.dy - 25,
                                        );
                                        isTapped = true;
                                      });
                                      // print(initialPosition);
                                    },
                                    onVerticalDragUpdate: (DragUpdateDetails details) {
                                      // print('${details.delta.dy}');
                                      setState(() {
                                        if (details.delta.dy < 0) {
                                          initialTappedPosition = Offset(initialTappedPosition.dx,
                                              initialTappedPosition.dy + details.delta.dy);
                                          height -= details.delta.dy;
                                        } else {
                                          height += details.delta.dy;
                                        }
                                        finalPosition = Offset(
                                          details.globalPosition.dx - 25,
                                          details.globalPosition.dy - 25,
                                        );
                                      });
                                    },
                                    onHorizontalDragUpdate: (DragUpdateDetails details) {
                                      // print('${details.delta.dx}');
                                      setState(() {
                                        if (details.delta.dx < 0) {
                                          initialTappedPosition = Offset(
                                            initialTappedPosition.dx + details.delta.dx,
                                            initialTappedPosition.dy,
                                          );
                                          width -= details.delta.dx;
                                        } else {
                                          width += details.delta.dx;
                                        }

                                        finalPosition = Offset(
                                          details.globalPosition.dx - 25,
                                          details.globalPosition.dy - 25,
                                        );
                                      });
                                    },
                                    onHorizontalDragEnd: (DragEndDetails details) {
                                      _determineWord();
                                    },
                                    onVerticalDragEnd: (DragEndDetails details) {
                                      _determineWord();
                                    },
                                  ),
                                ),
                              ),
                              Positioned(
                                top: initialTappedPosition.dy,
                                left: initialTappedPosition.dx,
                                child: Container(
                                  height: height,
                                  width: width,
                                  decoration: ShapeDecoration(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(30),
                                      side: BorderSide(
                                        color: isTapped ? Colors.yellow : Colors.transparent,
                                        width: 3.0,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          )
                      )
                  ),



                  Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                        margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                        height: 30,
                        width: 80,
                        decoration: ShapeDecoration(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30),
                              side: BorderSide(
                                  color: Colors.blue , width: 3.0),
                            ),
                            color: Colors.blue
                        ),
                        child:Center(
                          child: Text(' 3 / 13',style: TextStyle(color: Colors.white),),
                        )
                    ),
                  )

                ],
              ),
            ),

          ]
      ),
    );
  }

}