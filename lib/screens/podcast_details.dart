import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'dart:io' show Platform;
import 'package:http/http.dart' as http;
import 'package:crosswords/helpers/app_colors.dart';

import 'package:crosswords/helpers/common_methods.dart';
import 'package:crosswords/screens/reset_password.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:flutter_html/flutter_html.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'package:crosswords/helpers/app_constants.dart';
import 'dart:convert';
import 'package:crosswords/models/podcast_detail_model.dart';
import 'package:crosswords/models/check_model.dart';
import 'package:crosswords/screens/video_player.dart';

import 'package:crosswords/widgets/progress_bar.dart';
import 'package:crosswords/helpers/common_methods.dart';


class PodcastDetails extends StatefulWidget {

  PodcastDetails({Key key,this.id}) : super(key: key);
  final String id;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _PodcastDetailState();
  }
}

class _PodcastDetailState extends State<PodcastDetails> {

  var commonMethod = new CommonMethods();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  bool _loadingState= false;

  Future<ResultPodcastDetails> getDetail() async{
    setState(() {
      _loadingState = true;
    });

    String url = AppConstants.BASE_URL + 'dashboard/episodes?podcast_id='+ widget.id;
    print(url);
    final response = await http.get(url);
    print(response.body);
    return ResultPodcastDetails.fromJson(jsonDecode(response.body));
  }





  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomPadding: false,
      appBar: GradientAppBar(
        centerTitle: true,
        title: Text('Podcast Details'),
        gradient: LinearGradient(
            colors: [AppColors.PRIMARY_COLOR, AppColors.PRIMARY_COLOR, Colors.black]),
      ),
      body: new Stack(
          children: <Widget>[
            new Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(image: new AssetImage("assets/images/forgotPassword.png"), fit: BoxFit.cover,),
              ),
            ),

            new  FutureBuilder<ResultPodcastDetails>(
              future:getDetail(),
              builder: (context, snapshot) {

                if (snapshot.hasData) {
                  //return this.makeView(snapshot.data.result);
                  return Padding(
                    padding: EdgeInsets.all(10),
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Image.network (
                            AppConstants.BASE_URL_IMAGE + snapshot.data.result.podcast.podcast_image,
                            fit: BoxFit.cover,
                            height: MediaQuery.of(context).size.height * 0.3,
                            width: MediaQuery.of(context).size.width * 1 ,
                          ),
                          SizedBox(height: 10,),
                          Text(snapshot.data.result.podcast.name,style: TextStyle(color: Colors.white,fontSize: 18,fontWeight: FontWeight.w400),),
                          SizedBox(height: 10,),
                          Html(data:snapshot.data.result.podcast.description,
                            linkStyle: const TextStyle(
                            color: Colors.redAccent,
                            decorationColor: Colors.redAccent,
                            decoration: TextDecoration.underline,
                          ),),
                          SizedBox(height: 10,),
                          Text('Date : 28th March 2020',style: TextStyle(color: Colors.grey,fontSize: 15),),
                          SizedBox(height: 5,),
                          Divider(color: Colors.grey,thickness: 0.7,),
                          SizedBox(height: 5,),
                          Text('Episodes : ',style: TextStyle(color: Colors.white,fontSize: 18,fontWeight: FontWeight.w500),),
                          //episodes
                          new GridView.builder(
                            itemCount: snapshot.data.result.episodes.length,
                            shrinkWrap: true,
                            gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2,
                              childAspectRatio: MediaQuery.of(context).size.width /
                                  (MediaQuery.of(context).size.height/1.9 ),),
                            physics: NeverScrollableScrollPhysics() ,
                            itemBuilder: (BuildContext context, int index) {
                              return Container(
                                  margin: EdgeInsets.all(4),
                                  decoration: new BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: Colors.transparent,
                                  ),
                                  child:new GestureDetector(
                                    onTap: (){
                                      if( snapshot.data.result.episodes[index].episode_audio == null){
                                        commonMethod.showMessage('Video Coming Soon',error: false);
                                      }
                                      else{
                                        print('Sending--- ${snapshot.data.result.episodes[index].episode_image}');
                                        Navigator.push(context, new MaterialPageRoute(
                                          builder: (BuildContext context) => new VideoPlayer(videoUrl: AppConstants.BASE_URL_IMAGE + snapshot.data.result.episodes[index].episode_audio),
                                        ));
                                      }

                                    },
                                    child: Stack(
                                      children: <Widget>[
                                        Positioned(
                                          top: 0,bottom: 0,right: 0,left: 0,
                                            child: Image.network (
                                              AppConstants.BASE_URL_IMAGE + snapshot.data.result.episodes[index].episode_image,
                                              fit: BoxFit.fill,
                                            ),


                                        ),
                                        Positioned(
                                          bottom: 10,left: 10,
                                          child:Container(
                                            height: 30,width: 30,child:  Image.asset('assets/images/play.png',),
                                          ),
                                        )
                                      ],
                                    ),



                                  ));
                            },
                          )

                        ],
                      ),
                    ),
                  );
                } else if (snapshot.hasError) {
                  return Text("${snapshot.error}",style: TextStyle(color: Colors.white),);
                }
                // By default, show a loading spinner.
                return Container(
                  height: MediaQuery.of(context).size.height,
                  child: Center(
                      child: getSmallProgressBar()
                  ),

                );
              },
            )




          ]
      ),
    );
  }

  Widget makeView(data){

    var homeView =   new GridView.builder(
      itemCount: data.podcasts.length,
      shrinkWrap: true,
      gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2,
        childAspectRatio: MediaQuery.of(context).size.width /
            (MediaQuery.of(context).size.height/1.9 ),),
      physics: NeverScrollableScrollPhysics() ,
      itemBuilder: (BuildContext context, int index) {
        return Container(
            margin: EdgeInsets.all(4),
            decoration: new BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.transparent,
            ),
            child:new GestureDetector(
              child: Stack(
                children: <Widget>[
                  Positioned(
                    top: 0,bottom: 0,right: 0,left: 0,
                    child: Image.network (
                      AppConstants.BASE_URL_IMAGE + data.podcasts[index].podcast_image,
                      fit: BoxFit.fill,
                    ),
                  ),
                  Positioned(
                    bottom: 10,left: 10,
                    child:Container(
                      height: 30,width: 30,child:  Image.asset('assets/images/play.png',),
                    ),
                  )
                ],
              ),

              onTap: () {
                commonMethod.showMessage('Coming soon');
              },

            ));
      },
    );
    return homeView;
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value,style: TextStyle(color: Colors.white, fontSize: 15.0),),
      backgroundColor: Colors.red,
    ));
  }

}