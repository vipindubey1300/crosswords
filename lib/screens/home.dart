import 'dart:convert';

import 'package:crosswords/widgets/progress_bar.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'dart:io' show Platform;
import 'package:http/http.dart' as http;
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:crosswords/helpers/app_colors.dart';
import 'package:crosswords/helpers/app_constants.dart';
import 'package:crosswords/models/home_model.dart';
import 'package:crosswords/helpers/app_constants.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/services.dart';
import 'package:crosswords/screens/video_streaming.dart';

import 'package:crosswords/helpers/common_methods.dart';
import 'package:crosswords/screens/navigation_drawer.dart';
import 'package:crosswords/screens/podcast.dart';


class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomeState();
  }
}


class HomeState extends State<Home> {
  final homeScaffoldKey = GlobalKey<ScaffoldState>();


  bool _loadingState = false;
  String errorMessage;


  @mustCallSuper
  @override
  void initState() {

  }

  Future<ResultHome> getHomeData() async{
    setState(() {
      _loadingState = true;
    });

    var url = AppConstants.BASE_URL + 'dashboard/home';

    final response = await http.get(url);
    print(response.body);
    return ResultHome.fromJson(jsonDecode(response.body));
  }



  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      key: homeScaffoldKey,
      appBar: GradientAppBar(
        centerTitle: true,
        title: Text('Podcasts'),
        gradient: LinearGradient(
            colors: [AppColors.PRIMARY_COLOR, AppColors.PRIMARY_COLOR, Colors.black]),
        actions: <Widget>[

          new SizedBox(
            child: IconButton(
              padding: new EdgeInsets.all(8.0),
              icon: Icon(Icons.access_time, size: 20,),
              onPressed: () {
                Navigator.pushNamed(context, '/game');
              },
            ),
          ),
          new Container(
              padding: new EdgeInsets.fromLTRB(0, 20, 20, 0),
              child: Text("04:10" ,textAlign: TextAlign.right, style: TextStyle(
                fontWeight: FontWeight.bold, fontSize: 18,fontFamily: 'OpenSans',color:Colors.white,))
          ),


        ],
      ),
//
      //  drawer: NavigationDrawer(),
      body:  new Stack(
          children: <Widget>[
            new Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(image: new AssetImage("assets/images/forgotPassword.png"), fit: BoxFit.cover,),
              ),
            ),
            SingleChildScrollView(
                child:Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                          height: 200,width: MediaQuery.of(context).size.width,color: Colors.white,
                          child: GestureDetector(
                            onTap: (){
                              Navigator.push(context, new MaterialPageRoute(
                                builder: (BuildContext context) => new VideoStreaming(videoUrl: "https://www.webmobril.org/dev/crossword/uploads/video.mp4",),
                              ));
                            },
                            child: Stack(
                              children: <Widget>[
                                Image.asset('assets/images/main.jpg',fit: BoxFit.fitHeight,),
                                Positioned(
                                  bottom: 10,left: 10,
                                  child:Container(
                                    height: 30,width: 30,child:  Image.asset('assets/images/play.png'),
                                  ),
                                ),
                                Positioned(
                                    top: 10,right: 10,
                                    child:Text("Live",style: TextStyle(color:Colors.red,fontSize: 17,fontWeight: FontWeight.w700),)
                                ),
                              ],
                            ),
                          )
                      ),
                      SizedBox(height: 10,),
                      Container(
                        height: 50,width: MediaQuery.of(context).size.width,color: Colors.white,
                        child:TextFormField(
                          autofocus: false,
                          obscureText: false,
                          keyboardType: TextInputType.text,
                          style: TextStyle( fontSize: 20),
                          onSaved: (input) {
                            // _email = input;
                          },
                          decoration:InputDecoration(
                            contentPadding: EdgeInsets.only(top: 5.0),
                            border: InputBorder.none,
                            hintStyle: TextStyle(fontSize: 18.0, color: Colors.grey,fontFamily: 'OpenSans'),
                            icon: new Padding(
                                padding: EdgeInsets.fromLTRB(5, 8, 3,0),
                                child:Icon(
                                  Icons.search,size: 25,
                                  color: Colors.grey,
                                )),
                            hintText: "Search...",

                          ),//  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
                        ),
                      ),
                      SizedBox(height: 10,),
                      Container(
                          child: FutureBuilder<ResultHome>(
                            future:getHomeData(),
                            builder: (context, snapshot) {

                              if (snapshot.hasData) {
                                return this.makeHomeView(snapshot.data.result);
                              } else if (snapshot.hasError) {
                                return Text("${snapshot.error}");
                              }
                              // By default, show a loading spinner.
                              return Container(
                                height: 300,
                                width: 100,
                                child: Center(
                                  child: getSmallProgressBar(),
                                ),
                              );
                            },
                          )
                      )
                    ],

                  ),
                )
            )
          ]
      ),

      //Expanded(child: expandedChild)
    );

  }

  Widget makeHomeView(data){

    var homeView = Column(
      children: <Widget>[
        SizedBox(
        height: 100.0,
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: data.sliders.length,
            itemBuilder: (context, index){
                return InkWell(
                  onTap: () {
                    Fluttertoast.showToast(
                        msg:"Function Coming Soon...",
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.CENTER,
                        timeInSecForIos: 1,
                        backgroundColor: Colors.blueAccent,
                        textColor: Colors.white,
                        fontSize: 16.0
                    );
                  },
                  child: Card(
                    child: Image.network (
                      AppConstants.BASE_URL_IMAGE + data.sliders[index].slider_image,
                      fit: BoxFit.cover,width: 100,
                    )
                  ),
                );
            })
      ),
       SizedBox(height: 10,),
       Align(
         alignment: Alignment.centerLeft,
         child:  Text('Category Name',style: TextStyle(color: Colors.white,fontSize: 20,fontWeight: FontWeight.w500, fontFamily: "Opens Sans"),),
       ),
        SizedBox(height: 5,),

        new GridView.builder(
          itemCount: data.categories.length,
          shrinkWrap: true,
          gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2,
            childAspectRatio: MediaQuery.of(context).size.width /
              (MediaQuery.of(context).size.height/1.9 ),),
          physics: NeverScrollableScrollPhysics() ,
          itemBuilder: (BuildContext context, int index) {
            return Container(
                margin: EdgeInsets.all(4),
                decoration: new BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.transparent,
                ),
                child:new GestureDetector(
                    child: Stack(
                      children: <Widget>[
                        Positioned(
                          top: 0,bottom: 0,right: 0,left: 0,
                          child: Image.network (
                            AppConstants.BASE_URL_IMAGE + data.categories[index].image,
                            fit: BoxFit.fill,
                          ),
                        ),
                        Positioned(
                          bottom: 10,left: 10,
                          child:Container(
                            height: 30,width: 30,child:  Image.asset('assets/images/play.png',),
                          ),
                        )
                      ],
                    ),

                  onTap: () {
                    Navigator.push(context, new MaterialPageRoute(
                      builder: (BuildContext context) => new Podcast(
                          title: data.categories[index].name
                          ,id: data.categories[index].id),
                    ));
                  },

                ));
          },
        ),
        SizedBox(height: 10,),
        Align(
          alignment: Alignment.centerRight,
          child:  Text('',style: TextStyle(color: Colors.pink,fontSize: 18,fontWeight: FontWeight.w600),),
        ),
        SizedBox(height: 5,),

      ],
    );
    return homeView;
  }

  Future<bool> _onBackPressed() {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
          title: Text("Confirm Exit"),
         content: Text("Are you sure you want to exit?"),
           actions: <Widget>[
              FlatButton(
              child: Text("YES"),
              onPressed: () {
                 SystemNavigator.pop();
              },
              ),
          FlatButton(
          child: Text("NO"),
          onPressed: () {
          Navigator.of(context).pop();
          },
          )
        ],
        ),
      ) ??
        false;
  }
  Widget imageSlider() {

    return new Container(
        height: 250.0,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Container(



            ),

          ],
        )

    );
  }
}