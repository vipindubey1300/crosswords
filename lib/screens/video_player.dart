import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'dart:io' show Platform;
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:crosswords/helpers/app_colors.dart';
import 'package:crosswords/helpers/common_methods.dart';
import 'package:crosswords/screens/login.dart';
import 'package:video_player/video_player.dart';
import 'package:chewie/chewie.dart';


class VideoPlayer extends StatefulWidget {

  VideoPlayer({Key key, this.videoUrl}) : super(key: key);

  final String videoUrl;
  @override
  _VideoPlayerState createState() => new _VideoPlayerState();
}

class _VideoPlayerState extends State<VideoPlayer> {

  var commonMethod = new CommonMethods();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  VideoPlayerController playerController;
  VoidCallback listener;


  var chewieController = null;
  var videoPlayerController = null;

  var videoSource = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4";


  void createVideo() {
    if (playerController == null) {
      playerController = VideoPlayerController.network(
          "https://r3---sn-vgqsknez.googlevideo.com/videoplayback?source=youtube&mime=video%2Fmp4&itag=22&key=cms1&requiressl=yes&beids=%5B9466592%5D&ratebypass=yes&fexp=9466586,23724337&ei=g3jiWvfCL4O_8wScopaICA&signature=43C209DD37289D74DB39A9BBF7BC442EAC049426.14B818F50F4FA686C13AF5DD1C2A498A9D64ECC9&fvip=3&pl=16&sparams=dur,ei,expire,id,initcwndbps,ip,ipbits,ipbypass,itag,lmt,mime,mip,mm,mn,ms,mv,pl,ratebypass,requiressl,source&ip=54.163.50.118&lmt=1524616041346022&expire=1524813027&ipbits=0&dur=1324.768&id=o-AJvotKVxbyFDCz5LQ1HWQ8TvNoHXWb2-86a_50k3EV0f&rm=sn-p5qyz7s&req_id=e462183e4575a3ee&ipbypass=yes&mip=96.244.254.218&redirect_counter=2&cm2rm=sn-p5qe7l7s&cms_redirect=yes&mm=34&mn=sn-vgqsknez&ms=ltu&mt=1524791367&mv=m")
        ..addListener(listener)
        ..setVolume(1.0)
        ..initialize()
        ..play();
    } else {
      if (playerController.value.isPlaying) {
        playerController.pause();
      } else {
        playerController.initialize();
        playerController.play();
      }
    }
  }

  Widget makeVideo(){
    videoPlayerController = VideoPlayerController.network(widget.videoUrl);

    chewieController = ChewieController(
      videoPlayerController: videoPlayerController,
      aspectRatio: 1.2/ 2,
      autoPlay: true,
      looping: false,
      //fullScreenByDefault: true,
      allowFullScreen: true,
      showControls: true,
//      autoInitialize: true,
      // materialProgressColors: ChewieProgressColors(
      //   playedColor: Colors.red,
      //   handleColor: Colors.blue,
      //   backgroundColor: Colors.grey,
      //   bufferedColor: Colors.lightGreen,
      // ),
      // Try playing around with some of these other options:


      // materialProgressColors: ChewieProgressColors(
      //   playedColor: Colors.red,
      //   handleColor: Colors.blue,
      //   backgroundColor: Colors.grey,
      //   bufferedColor: Colors.lightGreen,
      // ),
      // placeholder: Container(
      //   color: Colors.grey,
      // ),
      // autoInitialize: true,
    );

    return Chewie(
      controller: chewieController,
    );
  }

  @override
  void initState() {
    super.initState();

  }

//
//  @override
//  void initState() {
//    super.initState();
//    playerController = VideoPlayerController.asset('assets/Butterfly-209.mp4');
//
//    playerController.addListener(() {
//      setState(() {});
//    });
//    playerController.setLooping(true);
//    playerController.initialize().then((_) => setState(() {}));
//    playerController.play();
//  }

  @override
  void dispose() {
//    playerController.setVolume(0.0);
//    playerController.removeListener(listener);
//    playerController.dispose();
//    super.dispose();

    // Ensure you dispose the VideoPlayerController to free up resources
    // _controller.dispose();
    print("-------------BEforee disponse----------------------------");

    videoPlayerController.dispose();
    chewieController.dispose();
    super.dispose();
    print("--------------After disponse-------------------");
  }



  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: _scaffoldKey,
      appBar: GradientAppBar(
        centerTitle: true,
        title: Text('Video'),
        gradient: LinearGradient(
            colors: [AppColors.PRIMARY_COLOR, AppColors.PRIMARY_COLOR, Colors.black]),
      ),
      body: new Stack(
          children: <Widget>[
              Container(
                child:  makeVideo(),
                color: Colors.black,
              )

          ]
      ),
    );
  }


}
