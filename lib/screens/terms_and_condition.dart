import 'package:crosswords/helpers/app_constants.dart';
import 'package:crosswords/widgets/progress_bar.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'dart:io' show Platform;
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

import 'package:crosswords/helpers/app_colors.dart';

import 'package:crosswords/helpers/common_methods.dart';
import 'package:crosswords/models/terms_and_condition_model.dart';

import 'package:flutter_html/flutter_html.dart';


class TermsAndConditions extends StatefulWidget {
  TermsAndConditions({Key key, this.title}) : super(key: key);
  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".



  final String title;
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _TermsAndConditionState();
  }
}

class _TermsAndConditionState extends State<TermsAndConditions> {

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  Future<TermsModel> termsandcondition() async {
    var url = AppConstants.BASE_URL + 'dashboard/infomative?page_id=2';
    final response = await http.get(url);
    print(termsModelFromJson(response.body));
    return termsModelFromJson(response.body);

  }




  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomPadding: false,
      appBar: GradientAppBar(
        centerTitle: true,
        title: Text('Terms and Conditions'),
        gradient: LinearGradient(
            colors: [AppColors.PRIMARY_COLOR, AppColors.PRIMARY_COLOR, Colors.black]),
      ),
      body: new Stack(
          children: <Widget>[
            new Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(image: new AssetImage("assets/images/forgotPassword.png"), fit: BoxFit.cover,),
              ),
            ),
           SingleChildScrollView(
             child:  Container(
               width: MediaQuery.of(context).size.width,
               child: FutureBuilder<TermsModel>(
                 future: termsandcondition(),
                 builder: (context, snapshot) {

                   if (snapshot.hasData) {
                     //     spacecrafts.add(snapshot.data.data.elementAt(0).categoryname);
                     return Container(
                       padding: const EdgeInsets.fromLTRB(10,20, 10, 10),
                       child:Html(
                         data:snapshot.data.result.page_description,
                         defaultTextStyle: TextStyle(fontSize: 17.5,fontWeight: FontWeight.normal,color: Colors.white),
                         linkStyle:  const TextStyle(fontSize: 12.5,fontWeight: FontWeight.bold,color: Colors.white),
                       ),
                       decoration: new BoxDecoration(
                         color: Colors.transparent,
                       ),
                       //     child: Text(spacecrafts[index], textAlign: TextAlign.center,style: TextStyle(fontSize: 12.5,fontWeight: FontWeight.bold),)
                     );

                   } else if (snapshot.hasError) {
                     return Text("${snapshot.error}");
                   }

                   // By default, show a loading spinner.
                   return new Container(
                     height: MediaQuery.of(context).size.height,
                     child: Center(
                       child: CircularProgressIndicator(),
                     ),
                   );
                 },
               ),
               padding: const EdgeInsets.fromLTRB(20,0, 0, 0),
             ) ,
           )



          ]
      ),
    );
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value,style: TextStyle(color: Colors.white, fontSize: 15.0),),
      backgroundColor: Colors.red,
    ));
  }

}