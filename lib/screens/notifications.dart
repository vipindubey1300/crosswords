import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'dart:io' show Platform;
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'dart:convert';
import 'package:crosswords/widgets/progress_bar.dart';
import 'package:crosswords/helpers/app_colors.dart';
import 'package:crosswords/helpers/app_constants.dart';
import 'package:crosswords/models/home_model.dart';
import 'package:crosswords/helpers/app_constants.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/services.dart';
import 'package:crosswords/helpers/common_methods.dart';
import 'package:crosswords/screens/login.dart';
import 'package:crosswords/models/notifications_model.dart';

class Notifications extends StatefulWidget {

  Notifications({Key key, this.title}) : super(key: key);
  final String title;
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _NotificationState();
  }
}

class _NotificationState extends State<Notifications> {

  var commonMethod = new CommonMethods();

  String userImage = "";

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  Future<NotificationResult> _fetchNotifications() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('jwt_token');
    String url = AppConstants.BASE_URL + 'dashboard/notifications?auth=' + token;

    final response = await http.get(url);
    print(response.body);
    return NotificationResult.fromJson(jsonDecode(response.body));
  }


  getUserImage() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
     var  image = prefs.getString('image');
     this.setState(() {userImage = image ;});
  }

  @override
  void initState() {
    super.initState();
    this.getUserImage();
  }


  @override
  Widget build(BuildContext context) {



    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomPadding: false,
      appBar: GradientAppBar(
        centerTitle: true,
        title: Text('Notifications'),
        gradient: LinearGradient(
            colors: [AppColors.PRIMARY_COLOR, AppColors.PRIMARY_COLOR, Colors.black]),
      ),
      body: new Stack(
          children: <Widget>[
            new Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(image: new AssetImage("assets/images/forgotPassword.png"), fit: BoxFit.cover,),
              ),
            ),

            Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: FutureBuilder<NotificationResult>(
                future: _fetchNotifications(),
                builder: (context,snapshot){
                  if(snapshot.hasData){
                    return ListView.builder(
                      shrinkWrap: true,
                      itemCount:snapshot.data.notifications.length,
                      itemBuilder: (context, index) {
                        return  new Card(
                          margin: const EdgeInsets.all(7.0),
                          color: AppColors.PRIMARY_COLOR,
                          elevation: 4,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16),
                            child: Row(

                              children: <Widget>[
                                CircleAvatar(backgroundColor: Colors.white,radius: 15,
                                  backgroundImage:  NetworkImage(AppConstants.BASE_URL_IMAGE + userImage,)),
                                SizedBox(width: 15,),
                                Flexible(
                                 // margin: EdgeInsets.only(left:10.0),

                                  child:  new Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      new Text(snapshot.data.notifications[index].message,
                                        overflow: TextOverflow.visible,
                                        softWrap: true,
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w100,
                                            fontStyle: FontStyle.normal,
                                            fontFamily: 'Open Sans',
                                            fontSize: 20),
                                      ),
                                      SizedBox(height: 7,),
                                      new Text(snapshot.data.notifications[index].added_date.substring(11,16), style: TextStyle(
                                          color: Colors.grey[700],
                                          fontStyle: FontStyle.normal,
                                          fontFamily: 'Open Sans',
                                          fontSize: 12),
                                      ),

                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),

                        );
                      },);
                  }
                  else if (snapshot.hasError) {
                    return Text("${snapshot.error}",style: TextStyle(color: Colors.white),);
                  }
                  return getSmallProgressBar();
                },
              ),
            )





          ]
      ),
    );
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value,style: TextStyle(color: Colors.white, fontSize: 15.0),),
      backgroundColor: Colors.red,
    ));
  }

}