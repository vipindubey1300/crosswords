import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'dart:io' show Platform;
import 'package:http/http.dart' as http;
import 'package:crosswords/helpers/app_colors.dart';

import 'package:crosswords/helpers/common_methods.dart';
import 'package:crosswords/screens/reset_password.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:crosswords/models/check_model.dart';

import 'package:crosswords/helpers/app_constants.dart';
import 'dart:convert';
import 'package:crosswords/models/login_model.dart';
import 'package:crosswords/widgets/progress_bar.dart';
import 'package:crosswords/helpers/common_methods.dart';



class VerifyOTP extends StatefulWidget {
  VerifyOTP({Key key, this.email,this.route}) : super(key: key);

  //0 means forgot password se aaya
  //1 means login se aaya
  //2 means register


  final String email;
  final int route;
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _OtpScreenState();
  }
}

class _OtpScreenState extends State<VerifyOTP> {

  getEmail (){
    return widget.email;
  }
  getRoute (){
    return widget.route;
  }

  final otpController = TextEditingController();

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  bool _loadingState = false;
  var commonMethod = new CommonMethods();


  @override
  void initState()  {
    super.initState();
    print(getEmail());
    print(getRoute());

  }

  void _verifyOtp(otp) async {

    String url = AppConstants.BASE_URL + 'auth/verifyotp';

    setState(() {
      _loadingState = true;
    });

    print(url);
    final response = await http.post(
        url,
        body: {
          'email':getEmail(),
          'otp': otp,
        }
    );
    setState(() {
      _loadingState = false;
    });

    var responseJson = jsonDecode(response.body);
    print(responseJson['status']);



    if(responseJson['status']) {
      var loginResponse = LoginModel.fromJson(responseJson);
      commonMethod.showMessage(loginResponse.message);

      if(getRoute() == 0){
        //forgot password
        Navigator.pop(context);
        Navigator.push(context, new MaterialPageRoute(builder: (BuildContext context) => new ResetPassword(email:getEmail()),));
      }
      else{

        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString('user_id', loginResponse.result.id.toString());
        prefs.setString('name', loginResponse.result.first_name);
        prefs.setString('email', loginResponse.result.email);
        prefs.setString('mobile', loginResponse.result.mobile);
        prefs.setString('jwt_token', loginResponse.result.jwt_token);
        prefs.setString('image', loginResponse.result.profile_pic);


        Navigator.pop(context);

      //  Navigator.pushNamed(context, '/home');
        Navigator.pushNamed(context, '/game');
      }
    }else if(!responseJson['status']){
      commonMethod.showMessage(responseJson['message'],error: true);
    }
    else{
      commonMethod.showMessage('Try Again..',error: true);
    }



  }

  void isValid(){
    var otp = otpController.text;
    //dismiss keyboard
    FocusScope.of(context).requestFocus(FocusNode());
    if(otp.isEmpty)
    {
      commonMethod.showMessage("Please Enter OTP",error: true);
    }
    else if (otp.length < 6){
      commonMethod.showMessage("Invalid OTP",error: true);
    }

    else {
      //commonMethod.showMessage("Login Successfully");
      _verifyOtp(otp);
      //login(_email,_password);
    }
  }

  void _resendOTP() async {
    String url = AppConstants.BASE_URL + 'auth/resendotp';
    setState(() {_loadingState = true;});

    final response = await http.post(url, body: {'email':widget.email,});
    setState(() {_loadingState = false;});
    try {
      var responseJson = jsonDecode(response.body);
      var res = CheckModel.fromJson(responseJson);
      print(response.body);

      if(res.status) {
        commonMethod.showMessage(res.message);
       // Navigator.push(context, new MaterialPageRoute(builder: (BuildContext context) => new VerifyOTP(email:email,route:0),));
      }else{
        commonMethod.showMessage(res.message,error: true);
      }

    } catch (e) {
      commonMethod.showMessage(e.toString(),error: true);
    }
  }


  @override
  Widget build(BuildContext context) {

    final otpField = Container(
        height: 50,
        width: MediaQuery.of(context).size.width/1.32,
        decoration: new BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.transparent,
        ),
        child:Padding (
            padding:const EdgeInsets.fromLTRB(10, 0,0, 0),
            child:TextFormField(
              textInputAction: TextInputAction.go,
              maxLength: 6,
              obscureText: true,
              controller: otpController,
                keyboardType: TextInputType.number,
                style: TextStyle( fontSize: 20,color: Colors.white),
               onSaved: (input) {
                // _email = input;
              },
                onChanged: (val) {
                  if (val.length == 6) { //10 is the length of the phone number you're allowing
                    FocusScope.of(context).requestFocus(FocusNode());
                    this.isValid();
                  }
                },
              decoration:InputDecoration(
                contentPadding: EdgeInsets.all(10),
                border: InputBorder.none,
                hintStyle: TextStyle(fontSize: 18.0, color: Colors.grey,fontFamily: 'OpenSans'),
                hintText: "Enter OTP",
                counterText: "",//to remove 0/6 marking in text inpuy

              ),//  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
            ))) ;




    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        centerTitle: true,
        title: const Text('OTP'),
        backgroundColor: AppColors.PRIMARY_COLOR,
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        behavior: HitTestBehavior.translucent,
        child: new Stack(
            children: <Widget>[
              new Container(
                decoration: new BoxDecoration(
                  image: new DecorationImage(image: new AssetImage("assets/images/forgotPassword.png"), fit: BoxFit.cover,),
                ),
              ),

              SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.fromLTRB(0, 40, 0, 0),
                  child: new Column(
                    children: <Widget>[
                      Container
                        (
                        width: MediaQuery.of(context).size.width,
                        height: 180,
                        child:Image.asset("assets/images/logoimage.png"),) ,
                      new Container(
                        margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                        child: Text(
                            "OTP sent to your register email id. Please enter OTP to recover your password.",
                            textAlign: TextAlign.center, style: TextStyle(
                          fontWeight: FontWeight.normal,
                          fontSize: 20,
                          fontFamily: 'OpenSans',
                          color: Colors.blueGrey,)),
                      ),
                      Container(
                        margin: const EdgeInsets.fromLTRB(20,20, 20, 10),
                        width: MediaQuery.of(context).size.width,
                        height: 100,
                        decoration: new BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black,
                              blurRadius: 30.0, // has the effect of softening the shadow
                              spreadRadius: 2.0, // has the effect of extending the shadow
                              offset: Offset(
                                10.0, // horizontal, move right 10
                                10.0, // vertical, move down 10
                              ),
                            ),],
                          color:AppColors.PRIMARY_COLOR,
                          borderRadius: new BorderRadius.circular(25.0),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [  //total container height is 150
                            SizedBox (height: 15.0),
                            otpField, //height is 50
                            Padding (
                              padding:const EdgeInsets.fromLTRB(10, 0,10, 0),
                              child:Divider(
                                color: Colors.white,
                                thickness: 0.5,
                                height: 1,
                              ),
                            ),
                            SizedBox (height: 15.0),
                          ],
                        ),
                      ),
                      Align(
                          alignment: Alignment.centerRight,
                          child: Material(
                            color: Colors.transparent,
                            child: InkWell(
                              child: Padding(
                                padding: EdgeInsets.all(10.0),
                                child:Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Text('Resend OTP',style: TextStyle(color: Colors.blue,fontSize: 16),),
                                    SizedBox(width: 5,),
                                  ],
                                ),
                              ),
                              onTap: (){
                                this._resendOTP();
                              },
                            ),
                          )
                      ),

                      new GestureDetector(
                        child: new Container(
                          margin: EdgeInsets.fromLTRB(0, 50, 0, 0),
                          width: 180,
                          height: 60,
                          child:new Padding(
                            padding: EdgeInsets.fromLTRB(5, 5, 5,5),
                            child:Center(child: Text(
                              'Send',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 19, fontFamily: 'Rock Salt',fontWeight: FontWeight.bold,

                              ),
                            ),),
                          ) ,
                          decoration: new BoxDecoration(
                            borderRadius:BorderRadius.circular(15.0),
                            image: new DecorationImage(
                              image: new AssetImage("assets/images/buttongradient.png",), fit: BoxFit.fill,
                            ),

                          ),
                        ),
                        onTap: ()
                        {
                          this.isValid();
                        },
                      ),

                    ],
                  ),
                ),
              ),


              _loadingState ?  getProgressBar() : Container()

            ]
        ),
      ),
    );
  }



}
class  EmptyAppBar  extends StatelessWidget implements PreferredSizeWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
  @override
  Size get preferredSize => Size(0.0,0.0);
}