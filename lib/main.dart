import 'package:flutter/material.dart';
import 'package:firebase_messaging/firebase_messaging.dart';


//screens
import 'package:crosswords/screens/splash.dart';
import 'package:crosswords/screens/login.dart';
import 'package:crosswords/screens/register.dart';
import 'package:crosswords/screens/change_password.dart';
import 'package:crosswords/screens/forgot_password.dart';
import 'package:crosswords/screens/home.dart';
import 'package:crosswords/screens/navigation_drawer.dart';
import 'package:crosswords/screens/reset_password.dart';
import 'package:crosswords/screens/verify_otp.dart';
import 'package:crosswords/screens/edit_profile.dart';
import 'package:crosswords/screens/support.dart';
import 'package:crosswords/screens/terms_and_condition.dart';
import 'package:crosswords/screens/notifications.dart';
import 'package:crosswords/screens/withdraw.dart';
import 'package:crosswords/screens/wallet.dart';
import 'package:crosswords/screens/add_account.dart';
import 'package:crosswords/screens/game.dart';
import 'package:crosswords/screens/video_player.dart';
import 'package:crosswords/screens/video_streaming.dart';
import 'package:fluttertoast/fluttertoast.dart';




void main() => runApp(MyApp());


class MyApp extends StatefulWidget {
  MyApp({Key key}) : super(key: key);
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp>{

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  void _navigateToItemDetail(Map<String, dynamic> message) {
    // Clear away dialogs
    Navigator.popUntil(context, (Route<dynamic> route) => route is PageRoute);
    // Navigator.push(context, item.route);
  }


  @override
  void initState(){
    super.initState();
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        //fires when the app is open and running in the foreground.
        print("onMessage-----: $message");
       // Fluttertoast.showToast(msg: message['notification']['title']);
        return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            content: ListTile(
              title: Text(message['notification']['title']),
              subtitle: Text(message['notification']['body']),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () => Navigator.of(context).pop(),
              ),
            ],
          ),
        );

      },
      onLaunch: (Map<String, dynamic> message) async {
        //fires if the app is closed, but still running in the background.
        print("onLaunch---: $message");

      },
      onResume: (Map<String, dynamic> message) async {
        //fires if the app is fully terminated.
        print("onResume------: $message");
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Crosswords',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Splash(),
      routes: <String, WidgetBuilder>{
        '/login':  (context) => new Login(),
        '/register':  (context) => new Register(),
        '/change_password':  (context) => new ChangePassword(),
        '/forgot_password':  (context) => new ForgotPassword(),
        '/home':  (context) => new Home(),
        '/navigation_drawer':  (context) => new NavigationDrawer(),
        '/reset_password':  (context) => new ResetPassword(email: null),
        '/verify_otp':  (context) => new VerifyOTP(),
        '/edit_profile':  (context) => new EditProfile(),
        '/support':  (context) => new Support(),
        '/terms_and_conditions':  (context) => new TermsAndConditions(),
        '/notifications':  (context) => new Notifications(),
        '/add_account':  (context) => new AddAccount(),
        '/wallet':  (context) => new Wallet(),
        '/withdraw':  (context) => new Withdraw(),
        '/game':  (context) => new Game(),
        '/video_player':  (context) => new VideoPlayer(),


      },//routes of the screen
      debugShowCheckedModeBanner: false,
    );
  }
}



