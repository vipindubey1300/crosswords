import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:crosswords/helpers/app_colors.dart';



Widget getProgressBar(){
  return Positioned(
      top: 0,left: 0,right: 0,bottom: 0,
      child: Container(
        color: Colors.black38,
        child: Center(
            child: SpinKitDualRing(
              color: Colors.blue,
            )
        ),
      )
  );
}

Widget getSmallProgressBar(){
  return SpinKitDualRing(
    color: Colors.blue,
  );
}
