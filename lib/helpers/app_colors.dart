import 'package:flutter/material.dart';

//new Container(color: const Color(0xff2980b9));
//new Color(0xAARRGGBB)
//AA = transparency
//
//RR = red
//
//GG = green
//
//BB = blue
//
//now if you want to create custom color 8-digit code from 6-digit color code then just append transparency (AA) value to it

/**
 * 100% - FF
    95% - F2
    90% - E6
    85% - D9
    80% - CC
    75% - BF
    70% - B3
    65% - A6
    60% - 99
    55% - 8C
    50% - 80
    45% - 73
    40% - 66
    35% - 59
    30% - 4D
    25% - 40
    20% - 33
    15% - 26
    10% - 1A
    5% - 0D
    0% - 00
 */

//So color is 0x then transparency like above FF and then html code without #



/// App Colors Class - Resource class for storing app level color constants
class AppColors {
  static const Color PRIMARY_COLOR = Color(0xFF0A212B);
  static const Color PRIMARY_COLOR_TRANSPARENT = Color(0x400A212B);
  static const Color COLOR_BLUE = Color(0xFF0859B5);
  static const Color MAROON = Color(0xFF950091);
  static const Color ACCENT_COLOR = Color(0xFF0A212B);
  static const Color SPLASH_BACKGROUND_COLOR = Color(0xFFB9FBF8);

}