import 'package:flutter/material.dart';
import 'package:date_format/date_format.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:io';
import 'dart:async';


import 'package:firebase_messaging/firebase_messaging.dart';



class CommonMethods {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Enter Valid Email';
    else
      return 'Valid';
  }
  String convertStringFromDate(DateTime todayDate ) {
    print(formatDate(todayDate, [yyyy, '-', mm, '-', dd]));
    return formatDate(todayDate, [yyyy, '-', mm, '-', dd]);
  }

  void convertDateFromString(String strDate){
    DateTime todayDate = DateTime.parse(strDate);
    print(todayDate);
    print(formatDate(todayDate, [yyyy, '/', mm, '/', dd]));
  }

  bool validateUsername(String value){
    String  pattern = r'^[a-zA-Z]+(\s[a-zA-Z]+)?$';
    RegExp regExp = new RegExp(pattern);
    return regExp.hasMatch(value);
  }


  String validateMobile(String value) {
    String patttern =  r'(^[0-9]*$)';
    RegExp regExp = new RegExp(patttern);
    if (!regExp.hasMatch(value)) {
      return 'Invalid';
    }
    return "Valid";
  }

  bool validatePassword(String value){
    String  pattern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{2,}$';
    RegExp regExp = new RegExp(pattern);
    return regExp.hasMatch(value);
  }

  void showMessage(String message,{bool error = false}){
     Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        backgroundColor: error ? Colors.red : Colors.blueAccent,
        textColor:  error ? Colors.white :Colors.white,
        fontSize: 16.0
    );
  }
  Future<String> getDeviceToken() async{

    if (Platform.isIOS) iOS_Permission();

  /**
    StreamSubscription iosSubscription;

    if (Platform.isIOS) {
      iosSubscription = _firebaseMessaging.onIosSettingsRegistered.listen((data) {
        // save the token  OR subscribe to a topic here
      });

    }
*/


    var token = await  _firebaseMessaging.getToken();
    assert(token != null);
    return token;
  }

  void iOS_Permission() {
    _firebaseMessaging.requestNotificationPermissions(const IosNotificationSettings(sound: true, badge: true, alert: true));

    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }





}